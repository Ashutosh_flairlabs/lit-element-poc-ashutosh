var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
import { BUTTON_VARIANT } from './type';
// background:  radial-gradient(circle, #47a7f5 1%);
class ButtonElement extends IDfyComponent {
    constructor() {
        super();
        this.type = "default";
        this.buttonName = "btn";
        this.btnHeight = "";
        this.btnWidth = "";
        this.onClick = () => { };
        this.size = 'medium';
        this.width = '100%';
        this.disabled = false;
        this.variant = BUTTON_VARIANT.FILLED;
        this.borderRadius = "0px";
    }
    static get styles() {
        return idfyCss `
    *{
      padding: 0;
      margin:0;
      font-family: 'Roboto', sans-serif;
      box-sizing: border-box;
  }
  .button { 
    display: inline-block;
    padding: 16px;
    font-size: var( --idfy-font-size,20px);
    cursor: pointer;
    text-align: var(--idfy-text-align,center);
    border-radius:var(--idfy-border-radius,4px);
    height:var(--idfy-height,54px); 
  }
  .button:hover {
    background-color: var(--color-700,#222B45);
    color: var(--brand-basic-100,#FFFFFF)
  }  
  
  .giant-text{
    //styleName: Button Font AA/ Giant;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: 24px;
    letter-spacing: 0px;
    text-align: center;
  }
  .medium-text{
    //styleName: Button Font AA/Medium;
 
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    letter-spacing: 0px;
    text-align: center;
  }
  .large-text{
    //styleName: Button Font AA/ Large;
   
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    letter-spacing: 0px;
    text-align: center;
  }
  .tiny-text{
    //styleName: Button Font AA/ Tiny;

    font-size: 10px;
    font-style: normal;
    font-weight: 700;
    line-height: 12px;
    letter-spacing: 0px;
    text-align: center;
  }
  .small-text{
    //styleName: Button Font AA/ Small;
   
      font-size: 12px;
      font-style: normal;
      font-weight: 700;
      line-height: 16px;
      letter-spacing: 0px;
      text-align: center;
  }
  .giant{
    height: 56px;
    width: 119px;
    left: 0px;
    top: 0px;
    border-radius: 4px;
    padding: 16px;
  }
  .large{
    height: 48px;
    width: 103px;
    left: 0px;
    top: 0px;
    border-radius: 4px;
    padding: 12px;
  }
  .medium{
    height: 40px;
    width: 91px;
    left: 0px;
    top: 0px;
    border-radius: 4px;
    padding: 10px;
  }
  .small{
    height: 32px;
    width: 80px;
    left: 0px;
    top: 0px;
    border-radius: 4px;
    padding: 8px 16px;
  }
  .tiny{
    height: 24px;
    width: 68px;
    left: 0px;
    top: 0px;
    border-radius: 4px;
    padding: 6px;
  }
  .success {
    --color-400: var(--brand-success-400); 
    --color-500: var(--brand-success-500);  
    --color-700: var(--brand-success-700); 
  }
  .primary {
    --color-400: var(--brand-primary-400); 
    --color-500: var(--brand-primary-500);  
    --color-700: var(--brand-primary-700);
    
  }
  .warning {
    --color-400: var(--brand-warning-400); 
    --color-500: var(--brand-warning-500);  
    --color-700: var(--brand-warning-700);
  }
  .danger {
    --color-400: var(--brand-danger-400); 
    --color-500: var(--brand-danger-500); 
    --color-700: var(--brand-danger-700); 
  }
  .error {
    --color-500: var(--brand-error-500);  
  }
  .info {
    --color-400: var(--brand-info-400); 
    --color-500: var(--brand-info-500);  
    --color-700: var(--brand-success-700); 
  }
  .filled {
    color: #ffff;
    background: var(--color-500,#222B45);
    border: 1px solid var(--color-500, #222B45);
  }
  .ghost{
    color: var(--color-500);
    background:none;
    border: none;
  }
  .outlined{
    color: var(--color-500);
    background: none;
    border: solid 1px var(--color-500);
  }

  /* Ripple effect */
  .press {
    background-position: center;
    transition: background 0.8s;
  }
  .press:active {
    background-color: var(--color-400,#6eb9f7);
    background-size: 100%;
    transition: background 0s;
  }
  
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    //${this.variant}
    getRender() {
        return tag `
        <button class="button ${this.type} ${this.variant} ${this.size} press"  ?disabled=${this.disabled} @click=${(e) => {
            const onChangeEvent = new CustomEvent('click', {
                detail: { "value": e }
            });
            dispatchEvent(onChangeEvent);
        }} part="button" style="border-radius:${this.borderRadius} ; height:${this.btnHeight} ; width:${this.btnWidth}">
          <div class="${this.size + '-text'}" >
          ${this.buttonName} 
          </div>
      </button>
  `;
    }
}
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "type", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "buttonName", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "borderRadius", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "btnHeight", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "btnWidth", void 0);
__decorate([
    idfyProperty({ type: BUTTON_VARIANT })
], ButtonElement.prototype, "variant", void 0);
__decorate([
    idfyProperty()
], ButtonElement.prototype, "disabled", void 0);
__decorate([
    idfyProperty({ type: Function })
], ButtonElement.prototype, "onClick", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "size", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonElement.prototype, "width", void 0);
customElements.define('button-element', ButtonElement);
//# sourceMappingURL=button-element.js.map