export interface ButtonCss {
    primary: string;
    secondary: string;
    borderRadius?: string;
    variant: string;
}
export declare enum BUTTON_VARIANT {
    FILLED = "filled",
    GHOST = "ghost",
    OUTLINED = "outlined"
}
//# sourceMappingURL=type.d.ts.map