var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
class ButtonGroup extends IDfyComponent {
    constructor() {
        super();
        this.data = [];
        this.buttonName = "btn";
        this.primary = "#000";
        this.secondary = "#000";
        this.borderRadius = "";
        this.variant = "";
        this.disabled = false;
        this.onClick = () => { };
    }
    static get styles() {
        return idfyCss `
    :root {
      --lit-button-font-color:#fff;
      --lit-button-bg-color:#fff;  
      --lit-button-border-color:#fff;
      --lit-button-border-radius:0px;
    }
    .btn-group button {
      display: inline-block;
      background-color:var(--lit-button-bg-color,#4189c4);
       border: 1px solid var(--lit-button-border-color,#fff);
      color:var(--lit-button-font-color,#fff);
      padding: 10px 24px;
      cursor: pointer; 
      float: left;
      border-radius:var(--lit-button-border-radius,10px); 
      font-size: var( --lit-button-font-size,20px);
      margin-left:3px;
    }'
    `;
    }
    update(changedProperties) {
        super.update(changedProperties);
        if (this.variant == "filled") {
            //bg
            this.style.setProperty("--lit-button-bg-color", this.primary);
            this.style.setProperty("--lit-button-font-color", this.secondary);
            this.style.setProperty("--lit-button-border-radius", this.borderRadius);
        }
        else if (this.variant == "ghost") {
            this.style.setProperty("--lit-button-bg-color", "#fff");
            this.style.setProperty("--lit-button-font-color", this.primary);
            this.style.setProperty("--lit-button-border-color", "#fff");
        }
        else {
            //outlined
            //font-color
            this.style.setProperty("--lit-button-font-color", this.primary);
            this.style.setProperty("--lit-button-border-color", this.primary);
            this.style.setProperty("--lit-button-bg-color", "#fff");
            this.style.setProperty("--lit-button-border-radius", this.borderRadius);
        }
    }
    toggle() {
        console.log('clicked');
    }
    // createElement();
    getRender() {
        console.log('buttonData>>', this.data);
        return tag `
    <h1>Button Group Element</h1>
  <div class="btn-group" disabled>
  ${this.data.map((item) => tag `
  <button ?disabled=${item.disabled} @click=${this.toggle} part="button" >${item.label}</button>
  `)}
</div>
  `;
    }
}
__decorate([
    idfyProperty({ type: Object })
], ButtonGroup.prototype, "data", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonGroup.prototype, "buttonName", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonGroup.prototype, "primary", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonGroup.prototype, "secondary", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonGroup.prototype, "borderRadius", void 0);
__decorate([
    idfyProperty({ type: String })
], ButtonGroup.prototype, "variant", void 0);
customElements.define('button-group', ButtonGroup);
//# sourceMappingURL=button-group.js.map