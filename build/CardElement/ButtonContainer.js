var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class ButtonContainer extends IDfyComponent {
    constructor() {
        super();
        this.buttonAlignment = "";
    }
    static get styles() {
        return idfyCss `
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        box-sizing: border-box;
    }
    .btnDiv{
       
        padding-top:16px;
        padding-right:24px;
        padding-bottom:17px;
        padding-left:24px;
        height:100%;
      
    }
    .btnDiv.right{
        flex-direction:row-reverse;
    }
    .btnDiv.left{
        flex-direction:row;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    firstUpdated() {
    }
    getRender() {
        return tag `
    <div class="btnDiv ${this.buttonAlignment}">
    <slot id="slot">
    
    </slot>
    </div>  
    `;
    }
}
__decorate([
    idfyProperty({ type: String })
], ButtonContainer.prototype, "buttonAlignment", void 0);
customElements.define('button-container-element', ButtonContainer);
//# sourceMappingURL=ButtonContainer.js.map