var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class CaptionText extends IDfyComponent {
    constructor() {
        super();
        this.caption = "Caption Text";
    }
    static get styles() {
        return idfyCss `
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        box-sizing: border-box;
    }
    .footer{
        padding:16px 24px 17px 24px;
        font-size:12px;
        line-height:16px;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    firstUpdated() {
    }
    getRender() {
        return tag `
    <div class="footer">
    <p>${this.caption}</p>
    </div>

    <div class="divider"></div>
    `;
    }
}
__decorate([
    idfyProperty({ type: String })
], CaptionText.prototype, "caption", void 0);
customElements.define('caption-text-element', CaptionText);
//# sourceMappingURL=CaptionText.js.map