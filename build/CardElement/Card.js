var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class Card extends IDfyComponent {
    constructor() {
        super();
        this.cardImg = "";
        this.title = "";
        this.subTitle = "";
        this.content = "";
        this.caption = "";
        //   @idfyProperty({ type: String }) buttonType1 = ""
        //   @idfyProperty({ type: String }) buttonType2 = ""
        this.fitToParent = false;
        this.buttonAlignment = "right";
        this.renderBtn = false;
        this.accent = false;
    }
    static get styles() {
        return idfyCss `
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        box-sizing: border-box;
    }
    .card{
      
        min-width:320px;
        width:100%;
        border-radius:4px;
        border:1px solid #EDF1F7;
        overflow:hidden;

        
    }
    .card.stretch{
        height:100%;        
    }

    .accent{
        width:100%;
        min-height:4px;
        height:4px;
        background-color:#1C43B9;
    }

    /*  Title and SubTitle Styles*/
    .title-div{
        padding:16px 24px 17px 24px;
        
    }
    .title{
       
    }
    .title p{
        line-height:24px;
        font-size:18px;

    }
    .sub-title{
       
    }
    .sub-title p{
        line-height:24px;
        font-size:13px;
    }
    .content-body{
        
    }
    .content-body{
       
    }
    .content-body.stretch{
        height:100%;
    }
    .content-body p{
        font-size:15px;
        padding:16px 24px 17px 24px;

    }
    .footer{
        padding:16px 24px 17px 24px;
    }
    .btnDiv{
       
        padding-top:16px;
        padding-right:24px;
        padding-bottom:17px;
        padding-left:24px;
    }
    .btnDiv slot.right{
       
        flex-direction:row-reverse;
    }
    .btnDiv slot.left{
        
        flex-direction:row;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
    .img{
        height:240px;
    }
    img{
        width:100%;
        height:100%;
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    contentBody() {
        return tag `
      

      <div class="content-body ${this.fitToParent ? "stretch" : ""}">
         <p>
         ${this.content}
         </p>
      </div>

      <div class="divider"></div>
      `;
    }
    captionText() {
        return tag `
    

    <div class="footer">
        <p>${this.caption}</p>
    </div>

    <div class="divider"></div>
    `;
    }
    titleDiv() {
        return tag `
    

    <div class="title-div">
        <div class="title">
            <p>${this.title}<p>
        </div>
        <div class="sub-title">
            <p>${this.subTitle}<p>
        </div>
    </div>

    <div class="divider"></div>
    `;
    }
    imageDiv() {
        return tag `
        <div class="img">
            <img src="${this.cardImg}">
        </div>

        <div class="divider"></div>
        `;
    }
    buttonDiv() {
        return tag `
       
        <div class="btnDiv">
            <slot id="slot" class="${this.buttonAlignment}">
            
            </slot>
        </div>
    
        `;
    }
    firstUpdated() {
        // const slot : any = this.renderRoot.querySelector("#slot") ;
        // slot.addEventListener('slotchange', function(e:any) {
        //     console.log(e.target);
        // })
        // if(this.slot){
        //     console.log("hi");
        // }
        // console.log(slot);
        // console.log(slot.btnDiv);
    }
    getRender() {
        return tag `
    <div class="card ${this.fitToParent ? "stretch" : ""}">
       ${this.accent ? tag `<div class="accent"></div>` : tag ``} 
        <slot>
        
        </slot>
    </div>

  `;
    }
}
__decorate([
    idfyProperty({ type: String })
], Card.prototype, "cardImg", void 0);
__decorate([
    idfyProperty({ type: String })
], Card.prototype, "title", void 0);
__decorate([
    idfyProperty({ type: String })
], Card.prototype, "subTitle", void 0);
__decorate([
    idfyProperty({ type: String })
], Card.prototype, "content", void 0);
__decorate([
    idfyProperty({ type: String })
], Card.prototype, "caption", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Card.prototype, "fitToParent", void 0);
__decorate([
    idfyProperty({ type: String })
], Card.prototype, "buttonAlignment", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Card.prototype, "renderBtn", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Card.prototype, "accent", void 0);
customElements.define('card-element', Card);
//# sourceMappingURL=Card.js.map