var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class CardBody extends IDfyComponent {
    constructor() {
        super();
        this.content = "A nebula is an interstellar cloud of dust, hydrogen, helium and other ionized gases.Nebula was a name for any diffuse astronomical object, including galaxies beyond the Milky Way.";
    }
    static get styles() {
        return idfyCss `
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        
        box-sizing: border-box;
    }
    .content-body{
        font-weight:400;
        height:100%;
       
    }
  
    .content-body p{
        font-size:15px;
        padding:16px 24px 17px 24px;
        line-height:20px;
       
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    firstUpdated() {
    }
    getRender() {
        return tag `
    <div class="content-body">
    <p>
    ${this.content}
    </p>
    </div>
    <div class="divider"></div>
    `;
    }
}
__decorate([
    idfyProperty({ type: String })
], CardBody.prototype, "content", void 0);
customElements.define('card-body-element', CardBody);
//# sourceMappingURL=CardBody.js.map