var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class CardImg extends IDfyComponent {
    constructor() {
        super();
        this.cardImg = "";
    }
    static get styles() {
        return idfyCss `
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        
        box-sizing: border-box;
    }
    img{
        width:100%;
        height:100%;
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
    .img{
        height:240px;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    firstUpdated() {
    }
    getRender() {
        return tag `
    <div class="img">
        <img src="${this.cardImg}">
    </div>

    <div class="divider"></div>
    `;
    }
}
__decorate([
    idfyProperty({ type: String })
], CardImg.prototype, "cardImg", void 0);
customElements.define('card-img-element', CardImg);
//# sourceMappingURL=CardImg.js.map