var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class Header extends IDfyComponent {
    constructor() {
        super();
        this.title = "";
        this.subTitle = "";
    }
    static get styles() {
        return idfyCss `
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        
        box-sizing: border-box;
    }
    .title-div{
        padding:16px 24px 17px 24px;
        font-weight:700;
    }
    .title{
        
    }
    .title p{
        line-height:24px;
        /*font-size:18px;*/
        font-size:15px;
    }
    .sub-title{
       
    }
    .sub-title p{
        line-height:24px;
        font-size:13px;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
    
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    firstUpdated() {
    }
    getRender() {
        return tag `
    <div class="title-div">
    <div class="title">
        <p>${this.title}<p>
    </div>
    <div class="sub-title">
        <p>${this.subTitle}<p>
    </div>
    </div>

    <div class="divider"></div>
    `;
    }
}
__decorate([
    idfyProperty({ type: String })
], Header.prototype, "title", void 0);
__decorate([
    idfyProperty({ type: String })
], Header.prototype, "subTitle", void 0);
customElements.define('header-element', Header);
//# sourceMappingURL=Header.js.map