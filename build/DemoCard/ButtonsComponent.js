import IDfyComponent, { tag, idfyCss } from '../IDfyComponent';
// import {ButtonCss} from './type'
class ButtonsComponent extends IDfyComponent {
    static get styles() {
        return idfyCss `
    *{
      padding: 0;
      margin:0;
      font-family: 'Roboto', sans-serif;
      box-sizing: border-box;
    }
   .button-container{
     width:100%;
     display:flex;
     align-items:center;
     justify-content:space-between;
     margin-top:16px;
   }
   .textarea{
    width:100%;
    height:100%;
    margin-top:16px;
   }
   .title{
     font-weight:700;
     font-size:15px;
     line-height:24px;
   }
   .container{
     display:flex;
     flex-direction:column;
     height:100%;
   }
  `;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    constructor() {
        super();
    }
    firstUpdated() {
    }
    getRender() {
        console.log("buttons component");
        return tag `
    <div class="container">
      <p class="title">Is the name of the BC location available on Board?</p>
      <div class="button-container">
        <div>
        <button-element  type="success" variant ="outlined" borderRadius ="4px" size="small" buttonName="Submit"  ></button-element> 
        </div>
        <div>
        <button-element  type="danger" btnWidth="48px" btnHeight="32px" variant ="ghost" borderRadius ="100px" size="small" buttonName="No"  ></button-element> 
        <button-element type="ghost" btnWidth="48px" btnHeight="32px" variant ="ghost" borderRadius ="100px"  size="small" buttonName="Yes"  ></button-element> 
        </div>

      </div>

      <div class = "textarea">

        <input-textarea
          type="text"
          class="stuff" 
          PlaceHolder="Enter your remarks here" 
          labelName="Enter REMARK"
          value=""
          captionText=""
          variant="default"
          maxLength = "50"
        ></input-textarea>

      </div>
    </div>

    `;
    }
}
customElements.define('buttons-component', ButtonsComponent);
//# sourceMappingURL=ButtonsComponent.js.map