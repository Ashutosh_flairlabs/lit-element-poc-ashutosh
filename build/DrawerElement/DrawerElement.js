var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { tag, idfyProperty } from '../IDfyComponent';
import { drawerStyle } from "./style";
class DrawerElement extends IDfyComponent {
    constructor() {
        super();
        this.open = "false";
        this.drawerWidth = "244px";
        this.anchor = "left";
        this.backGroundColor = "linear-gradient(180deg, #CE1417 0%, #1C43B9 30.33%)";
    }
    static get styles() {
        return drawerStyle;
    }
    update(changedProperties) {
        super.update(changedProperties);
        this.style.setProperty("--drawer-background", "linear-gradient(180deg, #CE1417 0%, #1C43B9 30.33%)");
    }
    toggleDrawer() {
        if (this.open === "false") {
            this.style.setProperty("--drawer-width", this.drawerWidth);
            this.open = "true";
            this.style.setProperty("--drawer-icon-position", this.drawerWidth);
        }
        else {
            this.style.setProperty("--drawer-width", '0%');
            this.style.setProperty("--drawer-icon-position", '0%');
            this.open = "false";
        }
        const toggleDrawer = new CustomEvent('toggle-drawer', {
            detail: { 'open': this.open }
        });
        this.dispatchEvent(toggleDrawer);
    }
    getRender() {
        return tag `
    <div class="drawer-container">
    <span  id="menu"  @click="${() => {
            this.toggleDrawer();
        }}">&#9776;</span>
        <div class="drawer" @click="${() => { this.toggleDrawer(); }}">
          <slot></slot>
        </div>
    </div>`;
    }
}
__decorate([
    idfyProperty({ type: String })
], DrawerElement.prototype, "open", void 0);
__decorate([
    idfyProperty({ type: String })
], DrawerElement.prototype, "drawerWidth", void 0);
__decorate([
    idfyProperty({ type: String })
], DrawerElement.prototype, "anchor", void 0);
__decorate([
    idfyProperty({ type: String })
], DrawerElement.prototype, "backGroundColor", void 0);
__decorate([
    idfyProperty({ type: Function })
], DrawerElement.prototype, "toggleDrawer", null);
customElements.define('drawer-element', DrawerElement);
//# sourceMappingURL=DrawerElement.js.map