import { idfyCss } from '../IDfyComponent';
export const drawerStyle = idfyCss `
body {
    font-family: "Lato", sans-serif;
  }
  #menu{
    font-size:30px;
    cursor:pointer;
    margin-left: var(--drawer-icon-position,0%);
    transition: 0.5s; right
  }
  .drawer {
    height: 1426px;
    width: var(--drawer-width,0%);
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    overflow-x: hidden;
    transition: 0.5s; right
    padding-top: 60px;
    background: var(--drawer-background,linear-gradient(180deg, #CE1417 0%, #1C43B9 30.33%));
  }
  
  .drawer a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s;
  }
  
  .drawer a:hover {
    color: #f1f1f1;
  }
  
  .drawer .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
  }
  
  @media screen and (max-height: 450px) {
    .drawer {padding-top: 15px;}
    .drawer a {font-size: 18px;}
  }
`;
//# sourceMappingURL=style.js.map