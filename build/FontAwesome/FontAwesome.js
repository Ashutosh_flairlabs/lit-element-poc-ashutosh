//import { LitElement, html} from 'lit-element';
import IDfyComponent, { tag, idfyICons } from '../IDfyComponent';
class FontAwesome extends IDfyComponent {
    getRender() {
        var _a, _b;
        var parser = new DOMParser();
        const linkTag = parser.parseFromString(idfyICons, 'text/html');
        const link = (_b = (_a = linkTag.firstChild) === null || _a === void 0 ? void 0 : _a.firstChild) === null || _b === void 0 ? void 0 : _b.firstChild;
        return tag `
  ${link}
  <div>
  <fa-icon  class="fas fa-address-card" color="#2980B9" size="2em"></fa-icon>
  <fa-icon class="fas fa-angle-double-down" color="#2980B9" size="2em"></fa-icon>
  </div>
   `;
    }
}
customElements.define('custom-component', FontAwesome);
//# sourceMappingURL=FontAwesome.js.map