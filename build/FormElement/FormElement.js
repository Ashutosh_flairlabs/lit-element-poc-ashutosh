var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
class FormElement extends IDfyComponent {
    constructor() {
        super();
        this.data = [];
        this.CheckList = [];
        this.data = [];
        this.toggleCheck = this.toggleCheck.bind(this);
        this.handleButton = this.handleButton.bind(this);
        this.radioCheck = this.radioCheck.bind(this);
        this.CheckList = [];
        this.radio = '';
        this.firstName = '';
        this.lastName = '';
    }
    static get styles() {
        return idfyCss `
        .form-controls{
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            border-radius: 5px;
            width: 500px;
            height:600px;
            padding-left: 40px;
            padding-top:40px;
        }
        .input-element{
          margin-bottom: 10px;
        }
        .label{
          margin-bottom: 10px;
        }
        .radio{
          margin-bottom: 20px;
          /* margin-left:50px; */
        }
        `;
    }
    _changeF_Name(e) {
        this.firstName = e.detail;
    }
    _changeL_Name(e) {
        this.lastName = e.detail;
    }
    // callBackFunction
    handleButton() {
        let myEvent = new CustomEvent('my-event', {
            detail: [{
                    checkboxDetails: this.CheckList,
                    gender: this.radio,
                    firstName: this.firstName,
                    lastName: this.lastName
                }],
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(myEvent);
    }
    radioCheck(e) {
        this.radio = e.detail;
    }
    toggleCheck(e) {
        this.CheckList = e.detail;
    }
    getRender() {
        return tag `
        <theme-provider>
    <form @submit=${this.handleButton}>
      <div class="form-controls">
      <div class="input-element">
      <multidropdown-elem  status="active" mutiDropdown= "true"  options = '["Option1","Option2","Option3","Option4","Option5","Option6","Option7"]'></multidropdown-elem > 

</br>
    
    </div>
    <div class="input-element">
    <icon-textfield placeHolder="First Name" imgUrl="public/Images/Icon.svg" labelName="First Name"> </icon-textfield>
    <icon-textfield placeHolder="Last Name" imgUrl="public/Images/Icon.svg" labelName="Last Name"> </icon-textfield>
    </div>
    <div class='label'>
    <label>Gender </label>
    </div>
    <div class="radio">
    <radio-theme @radio-event=${this.radioCheck} type ="primary"  data='[{"id": "1","text": "Male"},{"id": "2","text": "Female"}]'></radio-theme> 
    </div>
    <div class='label'>
    <label> Languages : </label>
    </div>
    <div class="radio">
    <checklist-theme @checklist-event=${this.toggleCheck} type="success"   data='[{"type":"","text":"JavaScript"},{"type":"checked","text":"CSS"},{"text":"React-js"},{"text":"React-Native"}]'></checklist-theme>
      </div>
    <button-element @click=${this.handleButton} type="primary" variant ="filled" borderRadius ="5px"  buttonName="Click Me"  ></button-element> 

      </div>
    </form>
    </theme-provider>
    `;
    }
}
__decorate([
    idfyProperty({ type: Object })
], FormElement.prototype, "data", void 0);
__decorate([
    idfyProperty({ type: Object })
], FormElement.prototype, "CheckList", void 0);
__decorate([
    idfyProperty({ type: String })
], FormElement.prototype, "radio", void 0);
__decorate([
    idfyProperty({ type: String })
], FormElement.prototype, "firstName", void 0);
__decorate([
    idfyProperty({ type: String })
], FormElement.prototype, "lastName", void 0);
// <button-element class='primary'  buttonName="Save"  @click=${this.handleButton} ></button-element>
customElements.define('form-element', FormElement);
// <input-lit-element class="stuff" @input-event=${this._changeF_Name} labelName='First Name '></input-lit-element>
//# sourceMappingURL=FormElement.js.map