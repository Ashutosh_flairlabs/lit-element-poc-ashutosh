import { LitElement, TemplateResult, PropertyValues } from 'lit';
import { property, query, queryAll } from 'lit/decorators';
import 'fa-icons';
export declare const tag: (strings: TemplateStringsArray, ...values: unknown[]) => TemplateResult<1>;
export declare const idfyCss: (strings: TemplateStringsArray, ...values: (number | import("lit").CSSResultGroup)[]) => import("lit").CSSResult;
export declare const idfyProperty: typeof property;
export declare const idfyQuery: typeof query;
export declare const idfyQueryAll: typeof queryAll;
export declare const idfyICons = "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.1/css/all.css\" \nintegrity=\"sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf\" \ncrossorigin=\"anonymous\"/>";
export declare type idfyPropertyValue = PropertyValues;
export default abstract class IDfyComponent extends LitElement {
    constructor();
    static styles: import("lit").CSSResult;
    abstract getRender(): TemplateResult;
    render(): TemplateResult<1 | 2>;
}
//# sourceMappingURL=IDfyComponent.d.ts.map