var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { eventOptions } from 'lit/decorators.js';
import IDfyComponent, { idfyProperty, tag, idfyQuery, idfyCss, idfyICons } from '../IDfyComponent';
import { VARIANT } from './type';
class IconTextfield extends IDfyComponent {
    constructor() {
        super();
        this.imgUrl1 = "";
        this.imgUrl2 = "";
        this.disabledImgUrl = "";
        this.icon = "";
        this.icon1 = "";
        this.icon2 = "";
        this.variant = VARIANT.DEFAULT;
        this.placeHolder = 'PlaceHolder';
        this.value = '';
        this.name = '';
        this.disabled = false;
        this.labelName = '';
        this.imgUrl = this.imgUrl1;
        this.captionText = '',
            this.helpText = '';
        this.type = "";
        // this.addEventListener('change', (event:any) => {
        //   console.log(event);
        // });
        // this.inputHandler = this.inputHandler.bind(this);
        // this.addEventListener('focus', this._handleFocus);
        // this._handleIcon = this._handleIcon.bind(this);
        this.updateInputType = this.updateInputType.bind(this);
    }
    // :host {
    //   --idfy-input-label-placeholder : #8F9BB3;
    //   --idfy-border-color:#E4E9F2;
    //   --idfy-caption-color:#8F9BB3;
    // }
    static get styles() {
        return idfyCss `
    #{
      padding: 0;
      margin:0;
      font-family: 'Roboto', sans-serif;
      box-sizing: border-box;
    }
    .div_container{          
      position:relative;
      width : 320px;
      height:88px;
   
     }
     .input_container { 
       position:absolute;
       padding:0;
       width:100%;
       top:24px;
       bottom:24px
    }
    .input { 
     padding:10px 16px;
     height:40px;
     width:100%;
     margin:0;
     padding-right:26px;
     background: #FFFFFF;
     border: 1px solid var(--idfy-border-color);
     box-sizing: border-box;
     border-radius: 4px;
    }
    input:focus {
      border: 1px solid var(--color-400,#1C43B9); 
      outline: 1px solid var(--color-400,#1C43B9); 
    }
    .input_img {
     position:absolute;
     top:10px;
     right:11px;
     width:20px;
     height:auto;
     cursor:pointer;
     
    }
    .input_img.disabled{
      pointer-events:none;
    }
    .label{

     font-style: normal;
     font-size:12px;
     line-height: 16px;
     color: var(--idfy-input-label-placeholder);
    }

    .label_right{
    margin-left:25px;
    
    font-style: normal;
    line-height: 16px;
    /* identical to box height, or 133% */
    /* Basic / 600 */
    color: var(--idfy-input-label-placeholder);
    }
    ::-webkit-input-placeholder{
    font-size:15px;
    line-height:20px;
    color: var(--idfy-input-label-placeholder);
    }
    .caption_image{
     bottom:3px;
     left:10px;
     width:20px;
     height:20px;
     vertical-align:top;
     /* display:inline-block;  */
     font-weight:bold;
    }
    .label_caption{
   
    font-style: normal;
    font-size: 12px;
    /* display:inline-block; */
    line-height: 16px; 
    color: var(--idfy-caption-color);
    position:absolute;
    bottom:0;
    /* text-align: right; */
    /* margin-left:5px; */
    }
    .lblResults1 {
  
    font-style: normal;
    font-weight: 700;
    font-size: 12px;


    top:0px;
    position:absolute;

    color: #8F9BB3;
    }
    .lblResults2 {
   
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    top:0px;
    right:0;
    position:absolute;
    color: #8F9BB3;
    /* width: 30%; */
    /* text-align: center; */
    } 
    .fa-eye-slash:before {
      color: #8F9BB3;
    }
    .fa-eye:before {
      color: #8F9BB3;
    }
    .default{
      --color-400: var(--brand-basic-400); 
      --color-500: var(--brand-basic-500);  
      --color-700: var(--brand-baic-700); 
    }
    .success {
      --color-400: var(--brand-success-400); 
      --color-500: var(--brand-success-500);  
      --color-700: var(--brand-success-700); 
    }
    .primary {
      --color-400: var(--brand-primary-400); 
      --color-500: var(--brand-primary-500);  
      --color-700: var(--brand-primary-700);
      
    }
    .warning {
      --color-400: var(--brand-warning-400); 
      --color-500: var(--brand-warning-500);  
      --color-700: var(--brand-warning-700);
    }
    .danger {
      --color-400: var(--brand-danger-400); 
      --color-500: var(--brand-danger-500); 
      --color-700: var(--brand-danger-700); 
    }
    .info {
      --color-400: var(--brand-info-400); 
      --color-500: var(--brand-info-500);  
      --color-700: var(--brand-success-700); 
    }
    .filled {
      color: #ffff;
      background: var(--color-500,#222B45);
      border: 1px solid var(--color-500, #222B45);
    }
    .ghost{
      color: var(--color-500);
      background:none;
      border: none;
    }
    .outlined{
      color: var(--color-500);
      background: none;
      border: solid 1px var(--color-500);
    }
    `;
    }
    set value(v) {
        const oldValue = v;
        this._value = v;
        this.requestUpdate('value', oldValue);
    }
    get value() {
        return this._value;
    }
    _handleChange() {
        // e.preventDefault();
        //console.log("input changed");
        //console.log(this.value);
        //  console.log(this.value);
        //   this.value = e.srcElement.value;
        //   console.log(this.value);
        // this.value = e;
        // const inputTextEvent = new CustomEvent('input-text-change-event', {
        //   detail: this.value,
        //   bubbles: true,
        //   composed: true
        // });
        // this.dispatchEvent(inputTextEvent);
    }
    updateInputType() {
        // const inputTextEvent = new CustomEvent('icon-click-event', {
        //   detail: {
        //     type : this.type
        //   },
        //   bubbles: true,
        //   composed: true
        // });
        // this.dispatchEvent(inputTextEvent); 
        if (this.type === "text") {
            //console.log(this.type);
            this.type = "password";
            this.imgUrl = this.imgUrl1;
            if (this.icon1) {
                this.icon = this.icon1;
            }
        }
        else if (this.type === "password") {
            //console.log(this.type);
            if (this.icon1 && this.icon2) {
                this.type = "text";
                this.imgUrl = this.imgUrl2;
            }
            if (this.icon2) {
                this.icon = this.icon2;
            }
        }
    }
    update(changedProperties) {
        super.update(changedProperties);
        if (this.disabled) {
            this.variant = VARIANT.BASIC;
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-400)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-400)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
            //console.log(this.disabledImgUrl);
            this.imgUrl = this.disabledImgUrl;
        }
        else if (this.variant == "default") {
            this.variant = VARIANT.BASIC;
            console.log(this.variant);
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-600)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-600)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
        }
        else if (this.variant == "success") {
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
            // this.style.setProperty("--lit-button-font-color",this.primary);
        }
        else if (this.variant == "error") {
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
            // this.style.setProperty("--lit-button-font-color",this.primary);
        }
        else if (this.variant == "warning") {
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
            // this.style.setProperty("--lit-button-font-color",this.primary);
        }
        else {
            //info
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
        }
    }
    firstUpdated() {
        //console.log(changedProperties);
        if (this.type == "password") {
            if (!this.icon1 && !this.icon2 && !this.imgUrl1 && !this.imgUrl2) {
                this.icon1 = "fas fa-eye-slash";
                this.icon2 = "far fa-eye";
            }
        }
        else {
            this.icon1 = `${this.icon1}`;
            this.icon2 = `${this.icon2}`;
        }
        this.imgUrl = this.imgUrl1;
        this.icon = this.icon1;
    }
    _inputHandler(e) {
        //console.log('change');
        this.value = e.target.value;
        const inputTextEvent = new CustomEvent('input-text-change-event', {
            detail: this.value,
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(inputTextEvent);
    }
    getRender() {
        var _a, _b;
        var parser = new DOMParser();
        const linkTag = parser.parseFromString(idfyICons, 'text/html');
        const link = (_b = (_a = linkTag.firstChild) === null || _a === void 0 ? void 0 : _a.firstChild) === null || _b === void 0 ? void 0 : _b.firstChild;
        return tag `
    ${link}
  
    <div class="div_container">
   
    <label id="lblResult1Desc" class="result lblResults1">${this.labelName} :</label>
    <label id="lblResult1Val" class="result lblResults2">${this.helpText}</label>
    <div class="input_container">
    
    ${this.type === 'text' ? tag `<img src=${this.imgUrl} class="input_img ${this.disabled ? "disabled" : ""}" @click=${this.updateInputType}  >`
            : tag `<img src=${this.imgUrl} class="input_img ${this.disabled ? "disabled" : ""}" @click=${this.updateInputType}>`} 

    ${this.type === 'text' ? tag `<fa-icon class="${this.icon} input_img ${this.disabled ? "disabled" : ""}" @click=${this.updateInputType}  >`
            : tag `<fa-icon class="${this.icon} input_img ${this.disabled ? "disabled" : ""}" @click=${this.updateInputType}  >`}

    <input class="input ${this.variant}" 
                type="${this.type}" 
                placeHolder="${this.placeHolder}" 
                .value="${this.value}"
                name="${this.name}" 
                ?disabled="${this.disabled}"
                @input=${(e) => this._inputHandler(e)}
    }>
    </input>
    </div>
    <label class="label_caption" >${this.captionText}</label>
    
    </div>`;
    }
}
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "_value", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "placeHolder", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], IconTextfield.prototype, "disabled", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "name", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "labelName", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "imgUrl", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "imgUrl1", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "imgUrl2", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "disabledImgUrl", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "captionText", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "helpText", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "type", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "icon", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "icon1", void 0);
__decorate([
    idfyProperty({ type: String })
], IconTextfield.prototype, "icon2", void 0);
__decorate([
    idfyProperty({ type: VARIANT })
], IconTextfield.prototype, "variant", void 0);
__decorate([
    idfyQuery('.input')
], IconTextfield.prototype, "_inputElement", void 0);
__decorate([
    eventOptions({ passive: true })
], IconTextfield.prototype, "getRender", null);
customElements.define('icon-textfield', IconTextfield);
// ${this.type === 'password' ? tag`<img src=${this.imgUrl} class="input_img" @click=${this.updateInputType}  >`
// : tag`<img src=${this.imgUrl} class="input_img" @click=${this.updateInputType}>`}
// @input=${(e: { target: { value: string; }; }) => { this.value = e.target.value; }}
//# sourceMappingURL=IconTextField.js.map