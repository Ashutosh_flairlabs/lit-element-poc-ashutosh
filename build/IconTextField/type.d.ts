export declare enum VARIANT {
    SUCCESS = "success",
    WARNING = "warning",
    INFO = "info",
    ERROR = "error",
    DEFAULT = "default",
    BASIC = "basic"
}
//# sourceMappingURL=type.d.ts.map