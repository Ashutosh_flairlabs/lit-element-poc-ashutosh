export declare enum VARIANT {
    SUCCESS = "success",
    WARNING = "warning",
    INFO = "info",
    ERROR = "error",
    DEFAULT = "default"
}
//# sourceMappingURL=type.d.ts.map