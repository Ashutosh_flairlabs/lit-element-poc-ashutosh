export var VARIANT;
(function (VARIANT) {
    VARIANT["SUCCESS"] = "success";
    VARIANT["WARNING"] = "warning";
    VARIANT["INFO"] = "info";
    VARIANT["ERROR"] = "error";
    VARIANT["DEFAULT"] = "default";
})(VARIANT || (VARIANT = {}));
//# sourceMappingURL=type.js.map