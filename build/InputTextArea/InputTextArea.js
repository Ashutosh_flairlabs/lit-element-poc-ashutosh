var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyQuery, idfyCss } from '../IDfyComponent';
import { VARIANT } from './type';
class InputTextArea extends IDfyComponent {
    constructor() {
        super();
        this.fitToParent = false;
        this.maxLength = "50";
        this.variant = VARIANT.DEFAULT;
        this.placeHolder = 'PlaceHolder';
        this.value = '';
        this.name = '';
        this.disabled = false;
        this.labelName = '';
        this.captionText = '',
            this.helpText = '';
        this.type = " ";
        this.addEventListener('focus', this._handleFocus);
        this._handleIcon = this._handleIcon.bind(this);
        this.updateInputType = this.updateInputType.bind(this);
    }
    static get styles() {
        return idfyCss `
    :host {
     
    }
    *{
      padding: 0;
      margin:0;
      font-family: 'Roboto', sans-serif;
      box-sizing: border-box;
    }
    .div_container{    
      position:relative;
      width : 320px;
      height:156px;
  
     }
     .div_container.stretch{
      width :100%;
      height:100%;
     }
     .input_container { 
       position:absolute;
       padding:0;
       width:100%;
      top:24px;
      bottom:24px;

    }
    .input_container.stretch{
      bottom:16px;
    }
    ::placeholder{
      color:#8F9BB3;
      font-weight:400;
      font-size:15px;
      line-height:20px;
    }
    .textarea { 
     padding:14px 16px;
     height:100%;
     width:100%;
     margin:0;
     background: #FFFFFF;
     border: 1px solid var(--idfy-border-color);
     border-radius: 4px;
     resize: none;
    }
    .textarea:focus{
      outline: 1px solid var(--idfy-border-color);
    }
    .input_img {
     position:absolute;
     top:10px;
     right:11px;
     width:20px;
     height:auto;
     cursor:pointer;
    }
    .label{

     font-size:12px;
     line-height: 16px;
     color: var(--idfy-input-label-placeholder);
    }

    .label_right{
    margin-left:25px;
  


    line-height: 16px;
    color: var(--idfy-input-label-placeholder);
    }

    .label_caption{
    
    font-size: 12px;
    /* display:inline-block; */
    line-height: 16px; 
   
    position:absolute;
    bottom:0;
    color:var(--inputTextArea-color-500);
    /* text-align: right; */
    /* margin-left:5px; */
    }

    .lblResults1 {

    font-weight: 700;
    font-size: 12px;
    top:0px;
    position:absolute;
    color:#8F9BB3;
    }

    .lblResults2 {

    font-weight: normal;
    font-size: 12px;
    top:0px;
    right:0;
    position:absolute;
    color: var(--inputTextArea-color-500);
    /* width: 30%; */
    /* text-align: center; */
    } 
    
    .success{
      --inputTextArea-color-500:var(--brand-success-500);
      
      
      
    }
    .warning{
      --inputTextArea-color-500:var(--brand-warning-500);
  
    }
    .primary{
      --inputTextArea-color-500:var(--brand-primary-500);
   
    }
    .error{
      --inputTextArea-color-500:var(--brand-error-500);
 
    }
    .danger{
      --inputTextArea-color-500:var(--brand-danger-500);
 
    }
    .info{
      --inputTextArea-color-500:var(--brand-info-500);

    }
    .disable{
      --inputTextArea-color-500:var(--brand-basic-500);

    }
    .basic{
      --inputTextArea-color-500:var(--brand-basic-500);
  
    }
    
    
    `;
    }
    _handleChange() {
        const inputTextEvent = new CustomEvent('input-text-event', {
            detail: this.value,
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(inputTextEvent);
    }
    _handleIcon() {
        console.log('Clicked');
    }
    updateInputType() {
        console.log({ type: this.type });
        const inputTextEvent = new CustomEvent('icon-click-event', {
            detail: {
                type: this.type
            },
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(inputTextEvent);
    }
    update(changedProperties) {
        super.update(changedProperties);
        // if(this.disabled){
        //   this.style.setProperty("--idfy-input-border",'var(--brand-'+this.variant+'-400)');
        //   this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-400)');
        //   this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
        // }
        // else if(this.variant == "default"){
        //   this.style.setProperty("--input-border","#E4E9F2");
        //   this.style.setProperty("--caption-color","#8F9BB3");
        // }
        // else if(this.variant == "success"){
        //   this.style.setProperty("--input-border","#00D68F");
        //   this.style.setProperty("--caption-color","#00D68F");
        // }
        // else if(this.variant == "danger"){
        //   this.style.setProperty("--input-border","#FF3D71");
        //   this.style.setProperty("--caption-color","#FF3D71");
        // }
        // else if(this.variant == "warning"){
        //   this.style.setProperty("--input-border","#FFAA00");
        //   this.style.setProperty("--caption-color","#FFAA00");
        // }
        // else{
        //   this.style.setProperty("--input-border","#0095FF");
        //   this.style.setProperty("--caption-color","#0095FF");
        // }
        if (this.disabled) {
            this.variant = VARIANT.BASIC;
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-400)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-400)');
            this.style.setProperty("--idfy-input-label-placeholder", 'var(--brand-' + this.variant + '-500)');
            //console.log(this.disabledImgUrl);
        }
        else if (this.variant == "default") {
            this.variant = VARIANT.BASIC;
            console.log(this.variant);
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-600)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-600)');
        }
        else if (this.variant == "success") {
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            // this.style.setProperty("--lit-button-font-color",this.primary);
        }
        else if (this.variant == "danger") {
            console.log(this.variant);
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            // this.style.setProperty("--lit-button-font-color",this.primary);
        }
        else if (this.variant == "warning") {
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
            // this.style.setProperty("--lit-button-font-color",this.primary);
        }
        else {
            //info
            this.style.setProperty("--idfy-border-color", 'var(--brand-' + this.variant + '-500)');
            this.style.setProperty("--idfy-caption-color", 'var(--brand-' + this.variant + '-500)');
        }
    }
    _handleFocus() {
        console.log('foused');
    }
    getRender() {
        console.log(this.maxLength);
        return tag `
    <div class="div_container ${this.variant} ${this.fitToParent ? "stretch" : ""}">
    <label id="lblResult1Desc" class="result lblResults1">${this.labelName} :</label>
    <label id="lblResult1Val" class="result lblResults2">${this.helpText}</label>
    <div class="input_container ${this.captionText ? "" : "stretch"}">
    
  
    <textarea class="textarea" 
                type="${this.type}" 
                name="${this.name}" 
                placeholder="${this.placeHolder}"
                value=""
                ?disabled="${this.disabled}"
                @input=${(e) => { this.value = e.target.value; }}
                @change=${this._handleChange}
                maxlength="${this.maxLength}"></textarea>
    </div>
    ${this.captionText ? tag `<label class="label_caption" >${this.captionText}</label>` : tag ``}
    
    </div>`;
    }
}
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "value", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "placeHolder", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], InputTextArea.prototype, "disabled", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], InputTextArea.prototype, "fitToParent", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "name", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "labelName", void 0);
__decorate([
    idfyProperty({ type: Function })
], InputTextArea.prototype, "callBackFunction", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "captionText", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "helpText", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "type", void 0);
__decorate([
    idfyProperty({ type: String })
], InputTextArea.prototype, "maxLength", void 0);
__decorate([
    idfyQuery('.input')
], InputTextArea.prototype, "_inputElement", void 0);
__decorate([
    idfyProperty({ type: VARIANT })
], InputTextArea.prototype, "variant", void 0);
customElements.define('input-textarea', InputTextArea);
//# sourceMappingURL=InputTextArea.js.map