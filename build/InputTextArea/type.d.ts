export declare enum VARIANT {
    SUCCESS = "success",
    WARNING = "warning",
    INFO = "info",
    ERROR = "danger",
    DEFAULT = "default",
    BASIC = "basic"
}
//# sourceMappingURL=type.d.ts.map