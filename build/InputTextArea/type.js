export var VARIANT;
(function (VARIANT) {
    VARIANT["SUCCESS"] = "success";
    VARIANT["WARNING"] = "warning";
    VARIANT["INFO"] = "info";
    VARIANT["ERROR"] = "danger";
    VARIANT["DEFAULT"] = "default";
    VARIANT["BASIC"] = "basic";
})(VARIANT || (VARIANT = {}));
//# sourceMappingURL=type.js.map