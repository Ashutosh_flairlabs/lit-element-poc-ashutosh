var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
class List extends IDfyComponent {
    constructor() {
        super();
        this.imgUrl = "";
        this.optionText = "";
        this.iconLeftAlign = false;
        this.disable = false;
        this.type = "primary";
    }
    firstUpdated() {
        //  console.log(this.renderRoot.querySelector("img")?.firstChild);
        //  console.log(this.renderRoot.querySelector("img"));
    }
    getRender() {
        return tag `
        <div class="list ${this.iconLeftAlign ? "left" : "right"} ${this.type} ${this.disable ? "disable" : ""}">
            <p>${this.optionText}</p>
            <img src="${this.imgUrl}">
        </div>

  `;
    }
}
List.styles = idfyCss `

    :host(:first-child) > * {
       border-top: 0px solid #fff !important;
       
    }

    :host > *{
       border-top:1px solid var(--button-color-500);
    }



    *{
      padding:0px;
      margin:0px;
      box-sizing:border-box;
    }
    p{
      margin:0px;
    }
    .list{
        background-color:var(--list-color-500,#f5f5f5);
        display:flex;
        cursor:pointer;
        align-items:center;
        padding:16px 18px;
    }

    .list.disable{
      cursor:not-allowed;
    }

    /*List left */

    .list.left{
        display:flex;
        flex-direction:row-reverse;
        justify-content:start;
        color:var(--color-500);
    }

    
    .list.left p {
        padding-left:8px;
      }

    .list.left img{
       /* margin-left:14px; */
      }
    

      /*List right */
    
    .list.right{
        display:flex;
        flex-direction:row;
        justify-content:start;
        color:var(--color-500);
      }
     
    
      .list.right img{
       margin-left:8px;
      }

      .success{
        --color-500:var(--brand-success-500);
      }
      .warning{
        --color-500:var(--brand-warning-500);
      }
      .primary{
        --color-500:var(--brand-primary-500);
      }
      .error{
        --color-500:var(--brand-error-500);
      }
      .info{
        --color-500:var(--brand-info-500);
      }
      .disable{
        --color-500:#8F9BB380;
        --list-color-500:var(--brand-basic-300);
      }
      .basic{
        --color-500:var(--brand-basic-800);
      }
    `;
__decorate([
    idfyProperty({ type: String })
], List.prototype, "imgUrl", void 0);
__decorate([
    idfyProperty({ type: String })
], List.prototype, "optionText", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], List.prototype, "iconLeftAlign", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], List.prototype, "disable", void 0);
__decorate([
    idfyProperty({ type: String })
], List.prototype, "type", void 0);
customElements.define('list-element', List);
//# sourceMappingURL=list.js.map