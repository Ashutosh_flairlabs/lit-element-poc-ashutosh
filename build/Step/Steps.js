var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
class Step extends IDfyComponent {
    // disable:Boolean;
    constructor() {
        super();
        this.disabled = false;
        this.completed = false;
        this.active = true;
        this.state = "completed";
        this.count = "0";
        this.stepsData = [];
        this.onClick = () => {
        };
        this.icon = "../public/done_icon.svg";
        this.index = 0;
        this.clickOnStep = () => {
            //console.log(this.stepsData);
            const stepCount = this.renderRoot.querySelector(".step-count");
            const stepState = this.renderRoot.querySelector(".step-state");
            if ((stepCount === null || stepCount === void 0 ? void 0 : stepCount.classList.contains("done")) && (stepState === null || stepState === void 0 ? void 0 : stepState.classList.contains("done"))) {
                this.active = true;
                //
                //this.completed=false;
            }
            const onChangeEvent = new CustomEvent('updateCurrentStep', {
                detail: { "value": this.count },
                bubbles: true,
                composed: true
            });
            this.dispatchEvent(onChangeEvent);
            this.requestUpdate();
            // this.active=true;
        };
        this.completed = false;
        this.active = false;
        // this.disable = false;
    }
    static get styles() {
        return idfyCss `
            :host {
                --active-border-color:#1C43B9;
                --active-color:#1C43B9;
                --active-bg-color:#1C43B914;
                --inactive-color:#8F9BB3;
                --step-font-size:12px;
            }
            .step{
                display: flex;
                flex-direction: column;
                align-items: center;
                font-family: Roboto;
                cursor:pointer;
                
            }
            .step.disabled{
               
                pointer-events: none;
            }

            /*step-count*/
            .step-count{
                display: flex;
                align-items: center;
                justify-content: center;
                height: 32px;
                width: 32px;
                font-size: var(--step-font-size);
                font-weight: 600;
                border-radius: 50%;
            }
            .step-count.active {
                border: 1px solid var(--active-color);
                background: var(--active-bg-color);
                color: var(--active-color);
            }

            /*Experimental*/ 
            .step-count.done.active {
                border: 1px solid var(--active-color);
                background: var(--active-bg-color);
                color: var(--active-color);
            }

            .step-count.inactive {
                border: 1px solid var(--inactive-color);
                color: var(--inactive-color);
            }
            .step-count.done {
                border: 1px solid var(--active-color);
                color: #fff;
                background: var(--active-color);
            }
            .step-count.done.disabled{
                cursor:not-allowed;
                pointer-events: none;
            }

            /*step-state*/
            .step-state.done.disabled {
                cursor:not-allowed;
                pointer-events: none;
            }
            .step-state.done {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }
            .step-state.active {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            } 
            /*Experimental*/ 
            .step-state.done.active {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }  
            .step-state.done.inactive {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }


            .step-state.inactive {
                color: var(--inactive-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }

        `;
    }
    getRender() {
        return tag `
            <div class="step ${this.disabled ? "disabled" : ""}" part="step" id = "${this.index}">
                <div class="step-count ${this.completed ? 'done' : ""} ${this.active ? 'active' : 'inactive'} ${this.disabled ? "disabled" : ""}" @click=${this.clickOnStep}>
                 ${this.active ? this.count : this.completed ? tag `<img class="step-icon" src=${this.icon}>` : this.count}
                </div> 
                <div class="step-state ${this.completed ? 'done' : ""} ${this.active ? 'active' : 'inactive'} ${this.disabled ? "disabled" : ""}">
                    ${this.active ? 'Active' : this.completed ? 'done' : 'Inactive'}
                </div>
            </div>
        `;
    }
}
__decorate([
    idfyProperty({ type: Boolean })
], Step.prototype, "disabled", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Step.prototype, "completed", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Step.prototype, "active", void 0);
__decorate([
    idfyProperty({ type: String })
], Step.prototype, "state", void 0);
__decorate([
    idfyProperty({ type: String })
], Step.prototype, "count", void 0);
__decorate([
    idfyProperty({ type: Array })
], Step.prototype, "stepsData", void 0);
__decorate([
    idfyProperty({ type: Function })
], Step.prototype, "onClick", void 0);
__decorate([
    idfyProperty({ type: String })
], Step.prototype, "icon", void 0);
__decorate([
    idfyProperty({ type: Number })
], Step.prototype, "index", void 0);
__decorate([
    idfyProperty({ type: Function })
], Step.prototype, "clickOnStep", void 0);
customElements.define('step-element', Step);
//# sourceMappingURL=Steps.js.map