var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// import {html} from 'lit-element';
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
class Stepper extends IDfyComponent {
    constructor() {
        super();
        // @idfyProperty({type:String})
        // idName = "#1"
        // @idfyQuery("step") _inputElement: any;
        this.onNext = () => {
            if (this.currentStep > this.steps.length - 1) {
                this.disabled = true;
            }
            if (this.currentStep == this.steps.length - 1) {
                this.steps[this.currentStep].isDone = true;
                this.steps[this.currentStep].isActive = false;
                this.nextLabel = 'SUBMIT';
                // let allSteps = this.renderRoot.querySelectorAll(".step-count");
                // console.log(allSteps);
                this.currentStep += 1;
            }
            if (this.currentStep < this.steps.length - 1) {
                this.steps[this.currentStep].isActive = false;
                this.steps[this.currentStep].isDone = true;
                this.currentStep += 1;
                this.steps[this.currentStep].isActive = true;
            }
            this.requestUpdate();
        };
        this.onPrev = () => {
            if (this.currentStep == 0) {
                this.steps[this.currentStep].isDone = false;
                this.steps[this.currentStep].isActive = true;
                this.nextLabel = 'NEXT';
            }
            if (this.currentStep > 0 && this.currentStep <= this.steps.length - 1) {
                this.steps[this.currentStep].isDone = false;
                if (this.steps[this.currentStep].isActive == false) {
                    this.steps[this.currentStep].isActive = true;
                    this.nextLabel = "NEXT";
                }
                else if (this.steps[this.currentStep].isActive == true) {
                    this.steps[this.currentStep].isActive = false;
                    this.currentStep -= 1;
                    this.steps[this.currentStep].isActive = true;
                    this.steps[this.currentStep].isDone = false;
                    this.nextLabel = "NEXT";
                }
            }
            this.requestUpdate();
        };
        this.onClick = (e) => {
            this.currentStep = e.target.count - 1;
            // this.disableBtn = false;
            this.steps.map((step) => {
                // console.log("inside map");
                if (step.isActive === true) {
                    step.isActive = false;
                    // step.isDone = true;
                }
                if (step.isDone) {
                    step.isDone = true;
                }
            });
            //    console.log(this.steps);
            this.steps[e.target.count - 1].isActive = true;
            // console.log(this.renderRoot.querySelector("."));
            // this.steps[e.target.count - 1].isDone = true;
            this.requestUpdate();
        };
        this.updateState = () => {
            this.requestUpdate();
        };
        this._setSteps = () => {
            console.log('set Steps event called');
        };
        this.steps = [
            {
                stepNo: 1,
                isActive: true,
                isDone: false,
                finalState: false
            },
            {
                stepNo: 2,
                isActive: false,
                isDone: false,
                finalState: false
            },
            {
                stepNo: 3,
                isActive: false,
                isDone: false,
                finalState: false
            }
        ];
        this.isActive = true;
        this.disabled = false;
        this.isDone = false;
        this.nextLabel = 'NEXT';
        this.prevLabel = 'PREVIOUS';
        this.currentStep = 0;
        this.icon = "";
        const onChangeEvent = new CustomEvent('getSteps', {
            detail: { "value": this.steps }
        });
        this.dispatchEvent(onChangeEvent);
        this.addEventListener('setSteps', this._setSteps);
        //   const setSteps = new CustomEvent('setSteps', {
        //     detail: { "value": this.steps  }
        //   });
        //   this.dispatchEvent(setSteps);
        // setInterval(() => {
        //     this.requestUpdate()
        // }, 1000)
    }
    // shouldUpdate(changedProperties : any) {
    //     changedProperties.forEach((oldValue:any, propName:any) => {
    //       console.log(`${propName} changed. oldValue: ${oldValue}`);
    //     });
    //     return changedProperties.has('prop1');
    //   }
    static get styles() {
        return idfyCss `
            .container {
                display: flex;
                flex-direction: column;
                border: 1px solid #c5c5c5;
            }
            .step-container {
                display: flex;
                justify-content: space-around;
                padding:10px;
                margin: 10px;
            }
            .disabled{
                cursor:not-allowed !important;
                pointer-events: none;
            }
            .btn-container{
                padding:10px;
                display: flex;
                justify-content: flex-end;
            }
            .btn {
                background-color: var(--active-color);
                box-shadow: 0px 8px 15px rgdisableStepba(0, 0, 0, 0.1);
                color: var(--btn-font-color);
                padding: 5px 10px;
                margin: 5px;
                font-weight: 700;
                border: none;
                border-radius: 4px;
                outline: none;
                cursor: pointer;
            }
            .btn.disable{
                cursor:not-allowed !important;
                pointe_handleFocus
        `;
    }
    getRender() {
        //    console.log("After" , this.steps);
        return tag `
        <div class="container" >
            <div class="step-container">
                ${this.steps.map((e, index) => tag `<step-element class="${this.disabled ? "disabled" : ""}" ?disabled=${this.disabled}  .values="${this.steps}" icon=${this.icon}  index=${index}  count=${e.stepNo} ?completed=${e.isDone} ?active=${e.isActive} @click = "${this.onClick}" ></step-element>`)}
            </div>
            <div class="btn-container">
              
            </div>
        </div>
        `;
    }
}
__decorate([
    idfyProperty({ type: Function })
], Stepper.prototype, "onNext", void 0);
__decorate([
    idfyProperty({ type: Function })
], Stepper.prototype, "onPrev", void 0);
__decorate([
    idfyProperty({ type: Function })
], Stepper.prototype, "onClick", void 0);
__decorate([
    idfyProperty({ type: Function })
], Stepper.prototype, "updateState", void 0);
__decorate([
    idfyProperty({ type: Function })
], Stepper.prototype, "_setSteps", void 0);
__decorate([
    idfyProperty({ type: Array })
], Stepper.prototype, "steps", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Stepper.prototype, "isActive", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Stepper.prototype, "disabled", void 0);
__decorate([
    idfyProperty({ type: Boolean })
], Stepper.prototype, "isDone", void 0);
__decorate([
    idfyProperty({ type: String })
], Stepper.prototype, "nextLabel", void 0);
__decorate([
    idfyProperty({ type: String })
], Stepper.prototype, "prevLabel", void 0);
__decorate([
    idfyProperty({ type: Number })
], Stepper.prototype, "currentStep", void 0);
__decorate([
    idfyProperty({ type: String })
], Stepper.prototype, "icon", void 0);
customElements.define('stepper-element', Stepper);
// <button class="btn ${this.disabled ? "disable" : ""}" part="step-btn" @click=${this.onPrev}>${this.prevLabel}</button>
// <button class="btn ${this.disabled ? "disable" : ""}" part="step-btn" @click=${this.onNext}>${this.nextLabel}</button>
//# sourceMappingURL=Stepper.js.map