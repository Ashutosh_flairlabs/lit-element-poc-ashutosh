var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag } from '../IDfyComponent';
import { themeStyle } from "./style";
const defaultTheme = {
    "brand-primary": {
        "100": "#D9E5FF",
        "200": "#7D9FE8",
        "300": "#5475D1",
        "400": "#305ACF",
        "500": "#1437A3",
        "600": "#1437A3",
        "700": "#0057C2",
        "800": "#0041A8",
        "900": "#002885",
    },
    "brand-transparant-primary": {
        "8": "#1C43B914",
        "16": "#1C43B929",
        "24": "#1C43B93D",
        "32": "#1C43B952",
        "40": "#1C43B966",
        "48": "#1C43B97A"
    }, "brand-success": {
        "100": "#F0FFF5",
        "200": "#CCFCE3",
        "300": "#8CFAC7",
        "400": "#2CE59B",
        "500": "#00D68F",
        "600": "#00B887",
        "700": "#00997A",
        "800": "#007D6C",
        "900": "#004A45",
    },
    "brand-warning": {
        "100": "#FFFDF2",
        "200": "#FFF1C2",
        "300": "#FFE59E",
        "400": "#FFC94D",
        "500": "#FFAA00",
        "600": "#DB8B00",
        "700": "#B86E00",
        "800": "#945400",
        "900": "#703C00"
    }, "brand-danger": {
        "100": "#FFF2F2",
        "200": "#FFD6D9",
        "300": "#FFA8B4",
        "400": "#FF708D",
        "500": "#FF3D71",
        "600": "#DB2C66",
        "700": "#B81D5B",
        "800": "#94124E",
        "900": "#700940"
    }, "brand-error": {
        "100": "#FFF2F2",
        "200": "#FFD6D9",
        "300": "#FFA8B4",
        "400": "#FF708D",
        "500": "#FF3D71",
        "600": "#DB2C66",
        "700": "#B81D5B",
        "800": "#94124E",
        "900": "#700940"
    },
    "brand-info": {
        "100": "#F2F8FF",
        "200": "#C7E2FF",
        "300": "#94CBFF",
        "400": "#42AAFF",
        "500": "#0095FF",
        "600": "#006FD6",
        "700": "#0057C2",
        "800": "#0041A8",
        "900": "#002885",
    }, "brand-basic": {
        "100": "#FFFFFF",
        "200": "#F7F9FC",
        "300": "#EDF1F7",
        "400": "#E4E9F2",
        "500": "#C5CEE0",
        "600": "#8F9BB3",
        "700": "#2E3A59",
        "800": "#222B45",
        "900": "#2E3A59",
        "1000": "#151A30",
        "1100": "#101426",
    }
};
class ThemeProvider extends IDfyComponent {
    constructor() {
        super();
        this.theme = defaultTheme;
    }
    static get styles() {
        return themeStyle;
    }
    update(changedProperties) {
        super.update(changedProperties);
        this.setAttributes(this.theme);
    }
    setAttributes(theme) {
        Object.keys(theme).forEach(brand => {
            Object.keys(theme[brand]).forEach((color) => {
                this.style.setProperty("--" + brand + "-" + color, theme[brand][color]);
            });
        });
    }
    getRender() {
        return tag `
            <div class='theme'>
            <slot></slot>
            </div>
        `;
    }
}
__decorate([
    idfyProperty({ type: Object })
], ThemeProvider.prototype, "theme", void 0);
__decorate([
    idfyProperty({ type: Function })
], ThemeProvider.prototype, "setAttributes", null);
customElements.define('theme-provider', ThemeProvider);
//# sourceMappingURL=ThemeProvider.js.map