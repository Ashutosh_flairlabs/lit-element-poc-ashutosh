var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { tag, idfyProperty } from '../IDfyComponent';
import { typographyStyle } from "./typographyStyle";
class TypographyElement extends IDfyComponent {
    constructor() {
        super();
        this.variant = "h1";
        this.component = "div";
    }
    static get styles() {
        ;
        return typographyStyle;
    }
    update(changedProperties) {
        super.update(changedProperties);
    }
    getRender() {
        console.log('ewngowE;NON', this.variant, this.component);
        return tag `
    <h1 class="${this.variant}" >
    <slot></slot>
    </h1>`;
    }
}
__decorate([
    idfyProperty({ type: String })
], TypographyElement.prototype, "variant", void 0);
__decorate([
    idfyProperty({ type: String })
], TypographyElement.prototype, "component", void 0);
customElements.define('typography-element', TypographyElement);
//# sourceMappingURL=TypographyElement.js.map