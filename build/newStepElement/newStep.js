var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
class newStep extends IDfyComponent {
    // disable:Boolean;
    constructor() {
        super();
        this.disabled = false;
        this.state = "inactive";
        this.finalState = "inactive";
        this.count = "0";
        this.onClick = () => {
        };
        this.icon = "../../public/done_icon.svg";
        this.index = 0;
        this.clickOnStep = () => {
            //console.log(this.stepsData);
            const stepCount = this.renderRoot.querySelector(".step-count");
            const stepState = this.renderRoot.querySelector(".step-state");
            if ((stepCount === null || stepCount === void 0 ? void 0 : stepCount.classList.contains("done")) && (stepState === null || stepState === void 0 ? void 0 : stepState.classList.contains("done"))) {
                this.state = "active";
                //
                //this.completed=false;
            }
            const onChangeEvent = new CustomEvent('updateCurrentStep', {
                detail: { "value": this.count },
                bubbles: true,
                composed: true
            });
            this.dispatchEvent(onChangeEvent);
            this.requestUpdate();
            // this.active=true;
        };
        // this.disable = false;
    }
    static get styles() {
        return idfyCss `
            :host {
                --active-border-color:#1C43B9;
                --active-color:#1C43B9;
                --active-bg-color:#1C43B914;
                --inactive-color:#8F9BB3;
                --step-font-size:12px;
            }
            :host(:last-child) .line{
               display:none;
             }
            *{
                padding:0;
                margin:0;
                @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
                box-sizing:border-box;
                font-family: 'Roboto', sans-serif;
              }
            .step-container{
                display:flex;
            }
            .step{
                display: flex;
                flex-direction: column;
                align-items: center;
                font-family: Roboto;
                cursor:pointer;
            
            }
            .step.disabled{
                pointer-events: none;
            }

            /*step-count*/
            .step-count{
                display: flex;
                align-items: center;
                justify-content: center;
                height: 32px;
                width: 32px;
                font-size: var(--step-font-size);
                font-weight: 700;
                border-radius: 16px;
            }
            .step-count.active {
                border: 1px solid var(--active-color);
                background: var(--active-bg-color);
                color: var(--active-color);
            }

            /*Experimental*/ 
            .step-count.done.active {
                border: 1px solid var(--active-color);
                background: var(--active-bg-color);
                color: var(--active-color);
            }

            .step-count.inactive {
                border: 1px solid var(--inactive-color);
                color: var(--inactive-color);
            }
            .step-count.done {
                border: 1px solid var(--active-color);
                color: #fff;
                background: var(--active-color);
            }
            .step-count.done.disabled{
                cursor:not-allowed;
                pointer-events: none;
            }

            /*step-state*/
            .step-state{
                font-weight: 700;
                margin-top:8px;
            }
            .step-state.done.disabled {
                cursor:not-allowed;
                pointer-events: none;
            }
            .step-state.done {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }
            .step-state.active {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            } 
            /*Experimental*/ 
            .step-state.done.active {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }  
            .step-state.done.inactive {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }


            .step-state.inactive {
                color: var(--inactive-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }
            .line{
                width:40px;
                height:1px;
                background-color:#EDF1F7;
                margin-top:15px;
                margin-left:auto;
                margin-right:auto;
            }

        `;
    }
    getRender() {
        // console.log(this.active);
        // console.log(this.state);
        return tag `
        <div class="step-container">
            <div class="step ${this.disabled ? "disabled" : ""}" part="step" id = "${this.index}">
                <div class="step-count ${this.finalState == "completed" ? 'done' : ""} ${this.state == "active" ? 'active' : 'inactive'} ${this.disabled ? "disabled" : ""}" @click=${this.clickOnStep}>
                 ${this.state == "active" ? this.count : this.finalState == "completed" ? tag `<img class="step-icon" src=${this.icon}>` : this.count}
                </div> 
                <div class="step-state ${this.finalState == "completed" ? 'done' : ""} ${this.state == "active" ? 'active' : 'inactive'} ${this.disabled ? "disabled" : ""}">
                    ${this.state == "active" ? 'Active' : this.finalState == "completed" ? 'done' : 'Inactive'}
                </div>
            </div>

        </div>
        `;
    }
}
__decorate([
    idfyProperty({ type: Boolean })
], newStep.prototype, "disabled", void 0);
__decorate([
    idfyProperty({ type: String })
], newStep.prototype, "state", void 0);
__decorate([
    idfyProperty({ type: String })
], newStep.prototype, "finalState", void 0);
__decorate([
    idfyProperty({ type: String })
], newStep.prototype, "count", void 0);
__decorate([
    idfyProperty({ type: Function })
], newStep.prototype, "onClick", void 0);
__decorate([
    idfyProperty({ type: String })
], newStep.prototype, "icon", void 0);
__decorate([
    idfyProperty({ type: Number })
], newStep.prototype, "index", void 0);
__decorate([
    idfyProperty({ type: Function })
], newStep.prototype, "clickOnStep", void 0);
customElements.define('new-step-element', newStep);
//# sourceMappingURL=newStep.js.map