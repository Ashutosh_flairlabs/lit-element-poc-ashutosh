export interface ButtonCss  {
    primary : string;
    secondary: string ;
    borderRadius? : string;
    variant : string 
  }

  export enum BUTTON_VARIANT { 
      FILLED = "filled",
      GHOST = "ghost",
      OUTLINED = "outlined"
  }
    // :host(.primary) button:active, :host(.primary) button:hover {
  //   --lit-button-bg-color: var(--primary-active, #0062cc);
  // }
  // :host(.secondary) button:active, :host(.secondary) button:hover {
  //   --lit-button-bg-color: var(--secondary-active, #0062cc);
  // }
//   .button[disabled]:hover {background-color: var(--lit-button-disabled-active-bg-color,#717679fa);  }
//    .button[disabled] {
//     background-color: var(--lit-button-disabled-bg-color,rgba(151, 150, 150, 0.932));
//     color: var(--lit-button-active-color);
//   }