import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

class ButtonGroup extends IDfyComponent {
  static override get styles() {
    return idfyCss`
    :root {
      --lit-button-font-color:#fff;
      --lit-button-bg-color:#fff;  
      --lit-button-border-color:#fff;
      --lit-button-border-radius:0px;
    }
    .btn-group button {
      display: inline-block;
      background-color:var(--lit-button-bg-color,#4189c4);
       border: 1px solid var(--lit-button-border-color,#fff);
      color:var(--lit-button-font-color,#fff);
      padding: 10px 24px;
      cursor: pointer; 
      float: left;
      border-radius:var(--lit-button-border-radius,10px); 
      font-size: var( --lit-button-font-size,20px);
      margin-left:3px;
    }'
    `;
  }
  update(changedProperties: any) {
    super.update(changedProperties)

    if (this.variant == "filled") {
      //bg
      this.style.setProperty("--lit-button-bg-color", this.primary);
      this.style.setProperty("--lit-button-font-color", this.secondary);
      this.style.setProperty("--lit-button-border-radius", this.borderRadius);

    }
    else if (this.variant == "ghost") {
      this.style.setProperty("--lit-button-bg-color", "#fff");
      this.style.setProperty("--lit-button-font-color", this.primary);
      this.style.setProperty("--lit-button-border-color", "#fff");

    }
    else {
      //outlined
      //font-color
      this.style.setProperty("--lit-button-font-color", this.primary);
      this.style.setProperty("--lit-button-border-color", this.primary);
      this.style.setProperty("--lit-button-bg-color", "#fff");
      this.style.setProperty("--lit-button-border-radius", this.borderRadius);
    }

  }
  @idfyProperty({ type: Object }) data = [];
  @idfyProperty({ type: String }) buttonName = "btn";
  @idfyProperty({ type: String }) primary = "#000";
  @idfyProperty({ type: String }) secondary = "#000";
  @idfyProperty({ type: String }) borderRadius = "";
  @idfyProperty({ type: String }) variant = "";

  constructor() {
    super();
    this.disabled = false;
    this.onClick = () => { }

  }


  disabled: boolean;
  onClick: () => void;
  toggle() {
   console.log('clicked');
  }
  // createElement();
  getRender() {
    console.log('buttonData>>', this.data);
    return tag`
    <h1>Button Group Element</h1>
  <div class="btn-group" disabled>
  ${this.data.map((item: any) => tag`
  <button ?disabled=${item.disabled} @click=${this.toggle} part="button" >${item.label}</button>
  `)}
</div>
  `;
  }
}

customElements.define('button-group', ButtonGroup);