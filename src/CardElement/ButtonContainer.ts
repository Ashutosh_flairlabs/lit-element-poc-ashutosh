import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

// import {ButtonCss} from './type'

class ButtonContainer extends IDfyComponent {
    

  static override get styles() {
   
    return idfyCss`
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        box-sizing: border-box;
    }
    .btnDiv{
       
        padding-top:16px;
        padding-right:24px;
        padding-bottom:17px;
        padding-left:24px;
        height:100%;
      
    }
    .btnDiv.right{
        flex-direction:row-reverse;
    }
    .btnDiv.left{
        flex-direction:row;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `}

  
    update(changedProperties : Map<string | number | symbol, unknown>) {
      super.update(changedProperties)

}

  constructor() {
    super();

  }

  @idfyProperty({ type: String }) buttonAlignment = "";

    firstUpdated(){
       
  }

  getRender() {
    return tag`
    <div class="btnDiv ${this.buttonAlignment}">
    <slot id="slot">
    
    </slot>
    </div>  
    `
  }
}

customElements.define('button-container-element', ButtonContainer);