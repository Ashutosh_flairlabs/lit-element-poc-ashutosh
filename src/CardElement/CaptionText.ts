import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

// import {ButtonCss} from './type'

class CaptionText extends IDfyComponent {
    

  static override get styles() {
   
    return idfyCss`
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        box-sizing: border-box;
    }
    .footer{
        padding:16px 24px 17px 24px;
        font-size:12px;
        line-height:16px;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `}

  
    update(changedProperties : Map<string | number | symbol, unknown>) {
      super.update(changedProperties)

}

  constructor() {
    super();

  }

  @idfyProperty({ type: String }) caption = "Caption Text";


    firstUpdated(){
       
  }

  getRender() {
    return tag`
    <div class="footer">
    <p>${this.caption}</p>
    </div>

    <div class="divider"></div>
    `
  }
}

customElements.define('caption-text-element', CaptionText);