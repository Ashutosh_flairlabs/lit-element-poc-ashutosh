import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

// import {ButtonCss} from './type'

class Card extends IDfyComponent {
    

  static override get styles() {
   
    return idfyCss`
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        box-sizing: border-box;
    }
    .card{
      
        min-width:320px;
        width:100%;
        border-radius:4px;
        border:1px solid #EDF1F7;
        overflow:hidden;

        
    }
    .card.stretch{
        height:100%;        
    }

    .accent{
        width:100%;
        min-height:4px;
        height:4px;
        background-color:#1C43B9;
    }

    /*  Title and SubTitle Styles*/
    .title-div{
        padding:16px 24px 17px 24px;
        
    }
    .title{
       
    }
    .title p{
        line-height:24px;
        font-size:18px;

    }
    .sub-title{
       
    }
    .sub-title p{
        line-height:24px;
        font-size:13px;
    }
    .content-body{
        
    }
    .content-body{
       
    }
    .content-body.stretch{
        height:100%;
    }
    .content-body p{
        font-size:15px;
        padding:16px 24px 17px 24px;

    }
    .footer{
        padding:16px 24px 17px 24px;
    }
    .btnDiv{
       
        padding-top:16px;
        padding-right:24px;
        padding-bottom:17px;
        padding-left:24px;
    }
    .btnDiv slot.right{
       
        flex-direction:row-reverse;
    }
    .btnDiv slot.left{
        
        flex-direction:row;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
    .img{
        height:240px;
    }
    img{
        width:100%;
        height:100%;
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
  `}

  
    update(changedProperties : Map<string | number | symbol, unknown>) {
      super.update(changedProperties)

}

  constructor() {
    super();

  }
  @idfyProperty({ type: String }) cardImg = "";
  @idfyProperty({ type: String }) title = "";
  @idfyProperty({ type: String }) subTitle = "";
  @idfyProperty({ type: String }) content = "";
  @idfyProperty({ type: String }) caption = "";
//   @idfyProperty({ type: String }) buttonType1 = ""
//   @idfyProperty({ type: String }) buttonType2 = ""
  @idfyProperty({ type: Boolean }) fitToParent = false;
  @idfyProperty({ type: String }) buttonAlignment = "right";
  @idfyProperty({ type: Boolean }) renderBtn = false;
  @idfyProperty({ type: Boolean }) accent = false;

  contentBody(){
      return tag`
      

      <div class="content-body ${this.fitToParent ? "stretch" : ""}">
         <p>
         ${this.content}
         </p>
      </div>

      <div class="divider"></div>
      `
  }

  captionText(){
    return tag`
    

    <div class="footer">
        <p>${this.caption}</p>
    </div>

    <div class="divider"></div>
    `
}

    titleDiv(){
    return tag`
    

    <div class="title-div">
        <div class="title">
            <p>${this.title}<p>
        </div>
        <div class="sub-title">
            <p>${this.subTitle}<p>
        </div>
    </div>

    <div class="divider"></div>
    `
}

    imageDiv(){
        return tag`
        <div class="img">
            <img src="${this.cardImg}">
        </div>

        <div class="divider"></div>
        `
    }

    buttonDiv(){
        return tag `
       
        <div class="btnDiv">
            <slot id="slot" class="${this.buttonAlignment}">
            
            </slot>
        </div>
    
        `
    }

    firstUpdated(){
        // const slot : any = this.renderRoot.querySelector("#slot") ;
        // slot.addEventListener('slotchange', function(e:any) {
        //     console.log(e.target);
            
        // })
        // if(this.slot){
        //     console.log("hi");
            
        // }
        // console.log(slot);
        // console.log(slot.btnDiv);
      
       
  }

  getRender() {

      
    return tag`
    <div class="card ${this.fitToParent ? "stretch" : ""}">
       ${this.accent? tag`<div class="accent"></div>` : tag``} 
        <slot>
        
        </slot>
    </div>

  `;
  }
}

customElements.define('card-element', Card);