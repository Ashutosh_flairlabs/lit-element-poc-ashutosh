import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

// import {ButtonCss} from './type'

class CardBody extends IDfyComponent {
    

  static override get styles() {
   
    return idfyCss`
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        
        box-sizing: border-box;
    }
    .content-body{
        font-weight:400;
        height:100%;
       
    }
  
    .content-body p{
        font-size:15px;
        padding:16px 24px 17px 24px;
        line-height:20px;
       
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `}

  
    update(changedProperties : Map<string | number | symbol, unknown>) {
      super.update(changedProperties)

}

  constructor() {
    super();

  }

  @idfyProperty({ type: String }) content = "A nebula is an interstellar cloud of dust, hydrogen, helium and other ionized gases.Nebula was a name for any diffuse astronomical object, including galaxies beyond the Milky Way.";



    firstUpdated(){
       
  }

  getRender() {
    return tag`
    <div class="content-body">
    <p>
    ${this.content}
    </p>
    </div>
    <div class="divider"></div>
    `
  }
}

customElements.define('card-body-element', CardBody);