import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

// import {ButtonCss} from './type'

class CardImg extends IDfyComponent {
    

  static override get styles() {
   
    return idfyCss`
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        
        box-sizing: border-box;
    }
    img{
        width:100%;
        height:100%;
        background-position: center; /* Center the image */
        background-repeat: no-repeat; /* Do not repeat the image */
        background-size: cover; /* Resize the background image to cover the entire container */
    }
    .img{
        height:240px;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
  `}

  
    update(changedProperties : Map<string | number | symbol, unknown>) {
      super.update(changedProperties)

}

  constructor() {
    super();

  }
  @idfyProperty({ type: String }) cardImg = "";



    firstUpdated(){
       
  }

  getRender() {
    return tag`
    <div class="img">
        <img src="${this.cardImg}">
    </div>

    <div class="divider"></div>
    `
  }
}

customElements.define('card-img-element', CardImg);