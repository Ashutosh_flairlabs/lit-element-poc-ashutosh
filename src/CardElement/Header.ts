import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';

// import {ButtonCss} from './type'

class Header extends IDfyComponent {
    

  static override get styles() {
   
    return idfyCss`
    /*  Card Styles*/
    *{
        padding: 0;
        margin:0;
        font-family: 'Roboto', sans-serif;
        
        box-sizing: border-box;
    }
    .title-div{
        padding:16px 24px 17px 24px;
        font-weight:700;
    }
    .title{
        
    }
    .title p{
        line-height:24px;
        /*font-size:18px;*/
        font-size:15px;
    }
    .sub-title{
       
    }
    .sub-title p{
        line-height:24px;
        font-size:13px;
    }
    .divider{
        background-color:#EDF1F7;
        width:100%;
        height:2px;
    }
    
  `}

  
    update(changedProperties : Map<string | number | symbol, unknown>) {
      super.update(changedProperties)

}

  constructor() {
    super();

  }
  @idfyProperty({ type: String }) title = "";
  @idfyProperty({ type: String }) subTitle = "";


firstUpdated(){
       
  }

  getRender() {
    return tag`
    <div class="title-div">
    <div class="title">
        <p>${this.title}<p>
    </div>
    <div class="sub-title">
        <p>${this.subTitle}<p>
    </div>
    </div>

    <div class="divider"></div>
    `
  }
}

customElements.define('header-element', Header);