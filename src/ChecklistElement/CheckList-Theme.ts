import IDfyComponent, { idfyCss, idfyProperty, tag } from '../IDfyComponent';

class CheckListTheme extends IDfyComponent {
  static override get styles() {
    return idfyCss`
    .container {
        display:block;
        position:relative;
        padding-left:30px;
        margin-bottom:10px;
        cursor:pointer;
        line-height: 24px;
        margin-left:10px;
        margin-top:10px; 
      } 
      /* Hide the default radio button */
      .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;  
      }
      /* custom radio button */
      .check {
            position: absolute;
            top: 0;
            left: 0;
            height: 21px;
            width:21px;
            background-color:var(--color-200,#C5CEE0);
            border-radius:3px;
            border:1px solid var(--color-200,#C5CEE0); 
      }
      .container:hover input ~ .check {
        z-index:1;
        border:1px  solid var(--color-500 ,#1C43B9);
        outline :solid 3px rgba(143, 155, 179, 0.16);
        }  
      .container input:checked ~ .check {
        background-color:var(--color-500,#1C43B9); /* checked bg color */
       }
      .check:after {
        content: "";
        position: absolute;
        display: none;
      }
      .container input:checked ~ .check:after {
        display: block;
      }
      .container .check:after {
        left:7px;
        top:4px;
        width: 4px;
        height: 8px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
      }
      
      .primary {
        --color-500: var(--brand-primary-500); 
        --color-200: var(--brand-primary-200);
      }
      .success {
        --color-500: var(--brand-success-500); 
        --color-200: var(--brand-success-200); 
      }
      .warning {
        --color-500: var(--brand-warning-500);
        --color-200: var(--brand-warning-200);  
      }
      .danger {
        --color-500: var(--brand-danger-500);
        --color-200: var(--brand-danger-200);  
      }
      .info {
        --color-500: var(--brand-info-500); 
        --color-200: var(--brand-info-200);  
      }
      .disabled{
        --color-500:var(--brand-basic-500);
        --color-200:var(--brand-basic-200); 
      }

    `
  }

  

  constructor() {
    super();
    this.data = [];
    this.toggleCheck = this.toggleCheck.bind(this);
    this.checklistData = [];
  }

  @idfyProperty({ type: Object }) data = [];
  @idfyProperty({ type: Object }) checklistData: any = [];
  @idfyProperty({ type: String }) type = "";
  @idfyProperty({ type: String }) fontColor = "";
  //create the customEvent & disaptch the event
  handleEvent() {
    let checklistEvent = new CustomEvent('checklist-event', {
      detail: this.checklistData,
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(checklistEvent);
  }
  //call the function create the checked array  
  private toggleCheck(e: any) {
    const value = (e.target as Element).getAttribute('value')!;
    if (e.target.checked) {
      this.checklistData[this.checklistData.length] = value;
    } else {
      const index = this.checklistData.indexOf(value);
      if (index > -1) {
        this.checklistData.splice(index, 1);
      }
    }
    this.handleEvent();
  }

  checkList() {
    this.data.map((item: any) => {
      if (item.type === 'checked') {
        this.checklistData[this.checklistData.length] = item.text;
        this.handleEvent();
      }
    });
  }
  getRender() {
    this.checkList();
    return tag`
        ${this.data.map((item: any) => tag`
        <label class="container ${this.type}">
        ${item.text}
            <input   type="checkbox" ?disabled=${item.disable} value=${item.text} ?checked=${item.type} @click=${this.toggleCheck}>
             <span class="check ${this.type}"></span>
            </label>  
         `)}
        `;
  }

}
customElements.define('checklist-theme', CheckListTheme);
