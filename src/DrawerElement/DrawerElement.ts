import IDfyComponent, { tag, idfyProperty } from '../IDfyComponent';
import {drawerStyle} from "./style"

class DrawerElement extends IDfyComponent {


    static override get styles() {
      return drawerStyle 
    }
  

  @idfyProperty({type: String}) open = "false";
  @idfyProperty({type: String}) drawerWidth = "244px"
  @idfyProperty({type: String}) anchor = "left";
  @idfyProperty({type: String}) backGroundColor = "linear-gradient(180deg, #CE1417 0%, #1C43B9 30.33%)";


  constructor() {
    super();
  }

  update(changedProperties : Map<string | number | symbol, unknown>) {
    super.update(changedProperties)
    this.style.setProperty("--drawer-background","linear-gradient(180deg, #CE1417 0%, #1C43B9 30.33%)")
}

@idfyProperty({type: Function})
toggleDrawer(){  
  if(this.open === "false"){    
    this.style.setProperty("--drawer-width",this.drawerWidth);
    this.open = "true";
    this.style.setProperty("--drawer-icon-position",this.drawerWidth)
 }
  else{
    this.style.setProperty("--drawer-width",'0%');
    this.style.setProperty("--drawer-icon-position",'0%')
    this.open = "false";
  }
  const toggleDrawer = new CustomEvent('toggle-drawer',{
    detail: {'open': this.open}
  })
  this.dispatchEvent(toggleDrawer);
}

  getRender() {
    return tag`
    <div class="drawer-container">
    <span  id="menu"  @click="${
      ()=>{
        this.toggleDrawer()
      }
    }">&#9776;</span>
        <div class="drawer" @click="${()=>{this.toggleDrawer()}}">
          <slot></slot>
        </div>
    </div>`
    ;
  }
}

customElements.define('drawer-element', DrawerElement);

