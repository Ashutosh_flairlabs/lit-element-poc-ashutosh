import IDfyComponent, {idfyProperty, tag , idfyCss} from '../IDfyComponent';
import { VARIANT } from './type';

class ExpandableButtonElem extends IDfyComponent {

    static styles = idfyCss`

    :host {
      --default-background-color : #00D68F;
      
      --default-down-icon-border:#00B887;
      --btn-ext-border:#00D68F;
    }
      *{
        padding:0;
        margin:0;
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
        box-sizing:border-box;
        font-family: 'Roboto', sans-serif;
      }

      /*styling for ext-button div */
      /*default background color*/
      .btn-ext{
        font-size: 14px;
        position: relative;
        display: inline-block;
        border:none;
        margin:50px;
        background-color:var(--button-color-500);
        cursor:pointer;
        border-radius: 4px 4px 4px 4px;
        transition: all 0.3s ease;
      }
      .btn-ext:hover{
        background-color:var(--button-color-600);
      }
      .btn-ext.disable{
        cursor:not-allowed;
      }

     /* basic styling for the button */
     
      .btn {
        display:flex;
        flex-direction: row;
        align-items:center;
        padding:0px;
        border-radius: 0px 4px 4px 0px;
        border:none;
        
      }


      /*styling for button when left alignment property is passed(chevron down on the left and text on the right)*/
      .btn.left{
        display:flex;
        flex-direction: row-reverse;
        align-items:center;
        padding:0px;
        color:#fff;
        
        position:relative;
      }
     
      .btn-ext left{
       
      }
      /*styling for options-div */
      .options{
        animation: fade_out_show 1.5s forwards;
        display:none;    
        border-radius: 0px 0px 8px 8px;
        color:#000;
        position: absolute;
        z-index: 1;
        overflow:hidden;
     
      }

      /* for drop-down */
      /* .options.down{
        bottom:-100%;
      }*/

      /* for drop-up */
      .options.up{
        bottom:100%;
        border-radius: 8px 8px 0px 0px;
      }
      /*converts from display none to block  */
      .options.add{ 
        display:block;
        animation: fade_in_show 0.5s forwards; 
       }

       /* options-div has multiple option divs */
      .option{
        background-color:#f5f5f5;
        border-bottom:1px solid var(--default-background-color);
        display:flex;
        width:100%;
        cursor:pointer;
        align-items:center;
        padding:14px 18px;
      }
      .option:hover{
        background-color:var(--)
      }
      /* css for option text (left) */
      .option.left{
        display:flex;
        flex-direction:row-reverse;
        justify-content:start;
      }

      .option.left p {
        padding-left:8px;
      }

      .option.left img{
        margin-left:14px;
      }

      /* css for option text (right) */
      .option.right{
        display:flex;
        flex-direction:row;
        justify-content:start;
      }

      .option.right p{
        padding-left:14px;
      }

     .option.right img{
       margin-left:8px;
      }

     .border-bottom{
       border-bottom:none;
      }
      /* the btn div is divided into 2 parts 1. text div 2. down icon div*/
      /* css for text-mark profile as */
     .text{
        display: flex;
        flex-direction: row;
        align-items: center;
        width:80%;
        color:var(--button-text-color);
        padding: 14px 18px;
        
      }


      /* arrow css */
      #arrow{
        transition: all 0.3s ease ;
      }

      #arrow.up{
        transform:rotate(180deg);
      }
      /* change arrow(chevron) style on click */
      #arrow.up.add{
        transform:rotate(360deg);
      }
      #arrow.add{
        transform:rotate(180deg);
      }


      /* down icon css */
      .down-icon{
        display:flex;
        align-items:center;
        justify-content:center;
        align-self:stretch;
        width:20%;
        border-left:1px solid var(--button-border-color);        
       
      }

    
      /* when down icon is on the left*/
      .down-icon.left{
        display:flex;
        align-items:center;
        align-self:stretch;
        width:20%;
        border-left:none;
        border-right:1px solid var(--button-border-color); 
             
      }

      @keyframes fade_in_show {
          0% {
                opacity: 0;
          }

          100% {
                opacity: 1;
          }
      }
      @keyframes fade_out_show {
          0% {
               display:flex;
                opacity: 1;   
                height:100%;  
          }
          100% {
                opacity: 0;
                height:0%; 
          }
      }

      .success{
        --button-color-500:var(--brand-success-500);
        --button-border-color:var(--brand-success-600);
        --button-color-600:var(--brand-success-600);
        --button-text-color:#fff;
      }
      .warning{
        --button-color-500:var(--brand-warning-500);
        --button-border-color:var(--brand-warning-600);
        --button-color-600:var(--brand-warning-600);
        --button-text-color:#fff;
      }
      .primary{
        --button-color-500:var(--brand-primary-500);
        --button-border-color:var(--brand-primary-800);
        --button-color-600:var(--brand-primary-600);
        --button-text-color:#fff;
      }
      .error{
        --button-color-500:var(--brand-error-500);
        --button-border-color:var(--brand-error-600);
        --button-color-600:var(--brand-error-600);
        --button-text-color:#fff;
      }
      .info{
        --button-color-500:var(--brand-info-500);
        --button-border-color:var(--brand-info-600);
        --button-color-600:var(--brand-info-600);
        --button-text-color:#fff;
      }
      .disable{
        --button-color-500:var(--brand-basic-300);
        --button-border-color:var(--brand-basic-400);
        --button-color-600:var(--brand-basic-300);
        --button-text-color:#8F9BB380;
      }
      .basic{
        --button-color-500:var(--brand-basic-300);
        --button-border-color:var(--brand-basic-600);
        --button-color-600:var(--brand-basic-600);
      }
    `;
  
 
   
    @idfyProperty({type:Boolean}) leftAlign=false;
    @idfyProperty({type:Boolean}) disable=false;
    @idfyProperty({type:String})  buttonBgColor = "#00D68F";
    @idfyProperty({type:String})  type = "success";
    @idfyProperty({type:String})  text = "Mark Profile As";
    @idfyProperty({type:Array})   options=[];
    @idfyProperty({type:Number})  width = 200;
    @idfyProperty({type:Number})  height = 50;
    @idfyProperty({type:Number})  buttonHeight = 0;
    @idfyProperty({type:Number})  buttonDistFromBottom = 0;
    @idfyProperty({type:Number})  optionsHeight = 0;
    @idfyProperty({type: VARIANT})
    variant: VARIANT = VARIANT.FILLED;
    @idfyProperty({type:Function}) onClick = ()=>{}
    
    btnExt: any;
    optionsEl: any;

    constructor() {
      super();
    }

 
   override firstUpdated() {
      //console.log(changedProperties);

      
      let getNodesInsideSlot = this.renderRoot.querySelector("slot")?.assignedNodes();
      
      let len  = (getNodesInsideSlot!.length) ;
      if(len % 2 == 0){
        //console.log(len/2);
        len = len/2;
        
      }else{
        //console.log((len - 1)/2);
        len = (len - 1)/2;
      }
     //whole button
      this.btnExt = this.renderRoot.querySelector(".btn-ext");
      
      //options
      //this.optionsEl  = this.renderRoot.querySelector('.options');
      // console.log(this.options,"options");
      
      if(this.btnExt){
      //get the space above the button
      this.buttonHeight =  this.btnExt.offsetTop;
      
      // distance from the bottom of the browser window
      this.buttonDistFromBottom = window.innerHeight - this.btnExt.offsetTop - this.btnExt.offsetHeight;
      // console.log(this.buttonDistFromBottom);
      
      //get the total height of the optionsDiv
      this.optionsHeight = this.btnExt.offsetHeight * len;
      // console.log(this.optionsHeight);  
        

      }

  
   }
   
   handleDropdownChange = (e: Object) => {
    const onChangeEvent = new CustomEvent('onClickOptions', {
      detail: { "value": e }
    });
    this.dispatchEvent(onChangeEvent);
    this._handleClick();
  }

   _handleClick(){
    let getNodesInsideSlot = this.renderRoot.querySelector("slot")?.assignedNodes();
      
    let len  = (getNodesInsideSlot!.length) ;
    if(len % 2 == 0){
      len = len/2;
      
    }else{
      len = (len - 1)/2;
    }

    //whole button
    this.btnExt = this.renderRoot.querySelector(".btn-ext");
    
    var scrollTop = this.btnExt.scrollTop();
    console.log(scrollTop);
    
    //options
    //this.optionsEl  = this.renderRoot.querySelector('.options');
    // console.log(this.options,"options");
    if(this.btnExt){
    //get the space above the button
    this.buttonHeight =  this.btnExt.offsetTop;
    console.log(window.innerHeight);
    
    // distance from the bottom of the browser window
    this.buttonDistFromBottom = window.innerHeight - this.btnExt.offsetTop - this.btnExt.offsetHeight;
    console.log(this.buttonDistFromBottom,"dist from bottom of the screen");
    
    //get the total height of the optionsDiv
    this.optionsHeight = this.btnExt.offsetHeight * len;
    console.log(this.optionsHeight,"height of options");  
    }
    //selected the options class
    const optionsDiv = this.renderRoot.querySelector('.options');




    // let firstNode = getNodesInsideSlot?.["1"];
    // console.log(firstNode);
    //makes the optionsDiv visible
    optionsDiv?.setAttribute("display","block");

    //select the arrow so that we can rotate the arrow on click
    const arrow  = this.renderRoot.querySelector('#arrow');
    arrow?.classList.toggle("add");
    optionsDiv?.classList.toggle("add");

  }

  _scroll() {
    console.log("hey");
    
  }

  getRender() {

    
    return tag`
    <div class="btn-ext ${this.disable?"disable":`${this.type}`} " @scroll="${this._scroll}">
      <div class="${this.leftAlign ? "btn left" : "btn"} ${this.disable ? "disable":""} " style="width:${this.width}px ;height: ${this.height}px;">
        <div class="text ${this.disable ? "disable":""}" style =";">${this.text}</div>
        <div class="${this.leftAlign ? "down-icon left" : "down-icon"} ${this.type} ${this.disable ? "disable":""}" @click=${() => {
            this.disable ? "" : this._handleClick();
          }}>
            <svg id="arrow" class="${this.optionsHeight > this.buttonDistFromBottom ? "up" : "down"}" width="10" height="7" viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M5.00001 6.50001C4.74401 6.50001 4.48801 6.40201 4.29301 6.20701L0.293006 2.20701C-0.0979941 1.81601 -0.0979941 1.18401 0.293006 0.793006C0.684006 0.402006 1.31601 0.402006 1.70701 0.793006L5.01201 4.09801L8.30501 0.918006C8.70401 0.535006 9.33501 0.546006 9.71901 0.943006C10.103 1.34001 10.092 1.97401 9.69501 2.35701L5.69501 6.21901C5.50001 6.40701 5.25001 6.50001 5.00001 6.50001Z" fill="${this.disable?"#8F9BB380":"#fff"}"/>
            </svg>
        </div>
      </div>
      
      <div class="options ${this.optionsHeight > this.buttonDistFromBottom ? "up" : "down"}" style="width:${this.width}px">
        <slot></slot>
      </div>
    </div>

  `;
  }

  }
customElements.define('expandable-button-element', ExpandableButtonElem);

// ${this.options.map((i:any,index:Number) => 
//   tag`
//   <div class="option ${this.leftAlign ?"left":"right"}"  id="${index}" style="width:${this.width}px ;height:${this.height}px ;color:${i.fontColor} ;" @click=${() => {
//   this.handleDropdownChange(i.value);
  
//     }}><p >${i.label}</p>
//     <img src="${i.icon}">
//     </div>
//     `)}