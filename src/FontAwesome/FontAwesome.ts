//import { LitElement, html} from 'lit-element';

import { TemplateResult } from 'lit';
import IDfyComponent, { tag , idfyICons} from '../IDfyComponent';

class FontAwesome extends IDfyComponent {
 getRender(): TemplateResult<1 | 2> {
  var parser = new DOMParser();
  const linkTag = parser.parseFromString(idfyICons, 'text/html'); 
  const link = linkTag.firstChild?.firstChild?.firstChild;

   return tag`
  ${link}
  <div>
  <fa-icon  class="fas fa-address-card" color="#2980B9" size="2em"></fa-icon>
  <fa-icon class="fas fa-angle-double-down" color="#2980B9" size="2em"></fa-icon>
  </div>
   `
 }
}

customElements.define('custom-component', FontAwesome );
