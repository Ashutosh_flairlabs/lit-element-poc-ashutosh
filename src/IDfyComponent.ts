import { LitElement, css, html, TemplateResult, PropertyValues } from 'lit';
import { property, query, queryAll } from 'lit/decorators';
import 'fa-icons';

export const tag = html;
export const idfyCss = css;
export const idfyProperty = property;
export const idfyQuery = query;
export const idfyQueryAll = queryAll;
export const idfyICons = `<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" 
crossorigin="anonymous"/>`

export type idfyPropertyValue = PropertyValues;

export default abstract class IDfyComponent extends LitElement {
    constructor() {
        super();
       
    }
    static override styles = css``;
    


    // abstract idfyRequestUpdate(): void;
    // override requestUpdate(){
    //     return this.idfyRequestUpdate();
    // }


    abstract getRender(): TemplateResult;
    // abstract getShouldUpdate?(changedProperties: PropertyValues): boolean;

    override render() {
        return this.getRender();
    }

    // protected override shouldUpdate(changedProperties: PropertyValues):boolean{
    //     return this.getShouldUpdate(changedProperties) 
    // }
}

