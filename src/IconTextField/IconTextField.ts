
import { TemplateResult } from 'lit';
import {eventOptions} from 'lit/decorators.js';
import IDfyComponent, { idfyProperty, tag, idfyQuery, idfyCss, idfyICons } from '../IDfyComponent';
import { VARIANT } from './type';
class IconTextfield extends IDfyComponent {
  
  // :host {
  //   --idfy-input-label-placeholder : #8F9BB3;
  //   --idfy-border-color:#E4E9F2;
  //   --idfy-caption-color:#8F9BB3;
  // }
  static override get styles() {
    return idfyCss`
    #{
      padding: 0;
      margin:0;
      font-family: 'Roboto', sans-serif;
      box-sizing: border-box;
    }
    .div_container{          
      position:relative;
      width : 320px;
      height:88px;
   
     }
     .input_container { 
       position:absolute;
       padding:0;
       width:100%;
       top:24px;
       bottom:24px
    }
    .input { 
     padding:10px 16px;
     height:40px;
     width:100%;
     margin:0;
     padding-right:26px;
     background: #FFFFFF;
     border: 1px solid var(--idfy-border-color);
     box-sizing: border-box;
     border-radius: 4px;
    }
    input:focus {
      border: 1px solid var(--color-400,#1C43B9); 
      outline: 1px solid var(--color-400,#1C43B9); 
    }
    .input_img {
     position:absolute;
     top:10px;
     right:11px;
     width:20px;
     height:auto;
     cursor:pointer;
     
    }
    .input_img.disabled{
      pointer-events:none;
    }
    .label{

     font-style: normal;
     font-size:12px;
     line-height: 16px;
     color: var(--idfy-input-label-placeholder);
    }

    .label_right{
    margin-left:25px;
    
    font-style: normal;
    line-height: 16px;
    /* identical to box height, or 133% */
    /* Basic / 600 */
    color: var(--idfy-input-label-placeholder);
    }
    ::-webkit-input-placeholder{
    font-size:15px;
    line-height:20px;
    color: var(--idfy-input-label-placeholder);
    }
    .caption_image{
     bottom:3px;
     left:10px;
     width:20px;
     height:20px;
     vertical-align:top;
     /* display:inline-block;  */
     font-weight:bold;
    }
    .label_caption{
   
    font-style: normal;
    font-size: 12px;
    /* display:inline-block; */
    line-height: 16px; 
    color: var(--idfy-caption-color);
    position:absolute;
    bottom:0;
    /* text-align: right; */
    /* margin-left:5px; */
    }
    .lblResults1 {
  
    font-style: normal;
    font-weight: 700;
    font-size: 12px;


    top:0px;
    position:absolute;

    color: #8F9BB3;
    }
    .lblResults2 {
   
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    top:0px;
    right:0;
    position:absolute;
    color: #8F9BB3;
    /* width: 30%; */
    /* text-align: center; */
    } 
    .fa-eye-slash:before {
      color: #8F9BB3;
    }
    .fa-eye:before {
      color: #8F9BB3;
    }
    .default{
      --color-400: var(--brand-basic-400); 
      --color-500: var(--brand-basic-500);  
      --color-700: var(--brand-baic-700); 
    }
    .success {
      --color-400: var(--brand-success-400); 
      --color-500: var(--brand-success-500);  
      --color-700: var(--brand-success-700); 
    }
    .primary {
      --color-400: var(--brand-primary-400); 
      --color-500: var(--brand-primary-500);  
      --color-700: var(--brand-primary-700);
      
    }
    .warning {
      --color-400: var(--brand-warning-400); 
      --color-500: var(--brand-warning-500);  
      --color-700: var(--brand-warning-700);
    }
    .danger {
      --color-400: var(--brand-danger-400); 
      --color-500: var(--brand-danger-500); 
      --color-700: var(--brand-danger-700); 
    }
    .info {
      --color-400: var(--brand-info-400); 
      --color-500: var(--brand-info-500);  
      --color-700: var(--brand-success-700); 
    }
    .filled {
      color: #ffff;
      background: var(--color-500,#222B45);
      border: 1px solid var(--color-500, #222B45);
    }
    .ghost{
      color: var(--color-500);
      background:none;
      border: none;
    }
    .outlined{
      color: var(--color-500);
      background: none;
      border: solid 1px var(--color-500);
    }
    `;
    
  }

  constructor() {
    super();
  
    this.variant = VARIANT.DEFAULT;
    this.placeHolder = 'PlaceHolder';
    this.value = '';
    this.name = '';
    this.disabled = false;
    this.labelName = '';
  
    this.imgUrl = this.imgUrl1;
    this.captionText = '',
    this.helpText = '';
    this.type = "";
    // this.addEventListener('change', (event:any) => {
    //   console.log(event);
      
    // });
    // this.inputHandler = this.inputHandler.bind(this);
    // this.addEventListener('focus', this._handleFocus);
    // this._handleIcon = this._handleIcon.bind(this);
    this.updateInputType = this.updateInputType.bind(this);



  }

  @idfyProperty({ type: String }) _value: any;
  // @idfyProperty({ type: Function }) _handleChange = ()=>{};
  @idfyProperty({ type: String }) placeHolder;
  @idfyProperty({ type: Boolean }) disabled;
  @idfyProperty({ type: String }) name;
  @idfyProperty({ type: String }) labelName;
  // @idfyProperty({ type: Function }) callBackFunction: any;
  @idfyProperty({ type: String }) imgUrl;
  @idfyProperty({ type: String }) imgUrl1="";
  @idfyProperty({ type: String }) imgUrl2="";
  @idfyProperty({ type: String }) disabledImgUrl = ""; 
  @idfyProperty({ type: String }) captionText;
  @idfyProperty({ type: String }) helpText;
  @idfyProperty({ type: String }) type;
  @idfyProperty({ type: String }) icon="";
  @idfyProperty({ type: String }) icon1 ="";
  @idfyProperty({ type: String }) icon2= "";

  @idfyProperty({type: VARIANT})
  variant: VARIANT;
  
  @idfyQuery('.input') _inputElement: HTMLDivElement | undefined;


  
  public set value(v : string) {
    const oldValue = v;
    this._value = v;

    this.requestUpdate('value',oldValue)
  }
  
  public get value(){
    return this._value
  } 

  _handleChange() {
    // e.preventDefault();
    //console.log("input changed");
    
    //console.log(this.value);
  //  console.log(this.value);
   
  //   this.value = e.srcElement.value;
  //   console.log(this.value);
    
    // this.value = e;
    // const inputTextEvent = new CustomEvent('input-text-change-event', {
    //   detail: this.value,
    //   bubbles: true,
    //   composed: true
    // });
    // this.dispatchEvent(inputTextEvent);
  }


  updateInputType() {
   
    // const inputTextEvent = new CustomEvent('icon-click-event', {
    //   detail: {
    //     type : this.type
    //   },
    //   bubbles: true,
    //   composed: true
    // });
    // this.dispatchEvent(inputTextEvent); 

    if(this.type === "text"){
      //console.log(this.type);
      this.type = "password";
      this.imgUrl = this.imgUrl1;
      
      if(this.icon1){
        this.icon = this.icon1;
      }
    }
    else if(this.type === "password"){
      //console.log(this.type);
      if(this.icon1 && this.icon2){
        this.type = "text";
        this.imgUrl = this.imgUrl2;
      }
      if(this.icon2){
        this.icon = this.icon2;
      }
      
    }

  }




  update(changedProperties : any) {
    super.update(changedProperties)

  if(this.disabled){
    this.variant = VARIANT.BASIC;
    
    
    this.style.setProperty("--idfy-border-color",'var(--brand-'+this.variant+'-400)');
    this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-400)');
    this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
    //console.log(this.disabledImgUrl);
    
    this.imgUrl = this.disabledImgUrl;
  }
  
  else if(this.variant == "default"){
    this.variant = VARIANT.BASIC;
    console.log(this.variant);
    
    this.style.setProperty("--idfy-border-color",'var(--brand-'+this.variant+'-600)');
    this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-600)');
    this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
  }
  else if(this.variant == "success"){
    
    
    this.style.setProperty("--idfy-border-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
    // this.style.setProperty("--lit-button-font-color",this.primary);


  }
  else if(this.variant == "error"){
    this.style.setProperty("--idfy-border-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
    // this.style.setProperty("--lit-button-font-color",this.primary);


  }
  else if(this.variant == "warning"){
    this.style.setProperty("--idfy-border-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
    // this.style.setProperty("--lit-button-font-color",this.primary);
  

  }
  else{
  //info
  this.style.setProperty("--idfy-border-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-caption-color",'var(--brand-'+this.variant+'-500)');
    this.style.setProperty("--idfy-input-label-placeholder",'var(--brand-'+this.variant+'-500)');
 
  }
}

firstUpdated() {
  //console.log(changedProperties);
  if(this.type == "password"){
    if(!this.icon1 && !this.icon2 && !this.imgUrl1 && !this.imgUrl2){
      this.icon1 = "fas fa-eye-slash";
      this.icon2 = "far fa-eye";
    }
   

  }else{
    this.icon1 =`${this.icon1}`
    this.icon2=`${this.icon2}`
  }
  this.imgUrl = this.imgUrl1;
  this.icon = this.icon1;
}
_inputHandler(e:any) {
  //console.log('change');
  this.value = e.target.value
  const inputTextEvent = new CustomEvent('input-text-change-event', {
      detail: this.value,
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(inputTextEvent);
  }

@eventOptions({passive: true})

    getRender(): TemplateResult<1 | 2> {
    var parser = new DOMParser();
    const linkTag = parser.parseFromString(idfyICons, 'text/html'); 
    const link = linkTag.firstChild?.firstChild?.firstChild;
    
    return tag`
    ${link}
  
    <div class="div_container">
   
    <label id="lblResult1Desc" class="result lblResults1">${this.labelName} :</label>
    <label id="lblResult1Val" class="result lblResults2">${this.helpText}</label>
    <div class="input_container">
    
    ${this.type === 'text' ? tag`<img src=${this.imgUrl} class="input_img ${this.disabled?"disabled":""}" @click=${this.updateInputType}  >`
        : tag`<img src=${this.imgUrl} class="input_img ${this.disabled?"disabled":""}" @click=${this.updateInputType}>`} 

    ${this.type === 'text' ? tag`<fa-icon class="${this.icon} input_img ${this.disabled?"disabled":""}" @click=${this.updateInputType}  >`
    : tag`<fa-icon class="${this.icon} input_img ${this.disabled?"disabled":""}" @click=${this.updateInputType}  >`}

    <input class="input ${this.variant}" 
                type="${this.type}" 
                placeHolder="${this.placeHolder}" 
                .value="${this.value}"
                name="${this.name}" 
                ?disabled="${this.disabled}"
                @input=${(e:any)=>this._inputHandler(e)}
    }>
    </input>
    </div>
    <label class="label_caption" >${this.captionText}</label>
    
    </div>`;
  }
}


customElements.define('icon-textfield', IconTextfield);



// ${this.type === 'password' ? tag`<img src=${this.imgUrl} class="input_img" @click=${this.updateInputType}  >`
// : tag`<img src=${this.imgUrl} class="input_img" @click=${this.updateInputType}>`}

// @input=${(e: { target: { value: string; }; }) => { this.value = e.target.value; }}