import IDfyComponent, { idfyProperty, tag, idfyCss } from '../IDfyComponent';
import { VARIANT } from './type';

class InputLitElement extends IDfyComponent {
    static override get styles() {
        return idfyCss`
            :host {
                --input-border:#E4E9F2;
                --input-label-placeholder :#8F9BB3;
                
            }
            :host {
            color: black;  
            }
            .input{
                position:absolute;
                bottom:24px;
                top:24px;
                width:100%;
                padding:10px 16px ;
                border:1px solid var(--input-border);
                border-radius: 4px;
            }
            .input{
                border:1px solid var(--input-border);
            }
            .input:focus{
                outline: 1px solid var(--input-border);
            }

            ::-webkit-input-placeholder{
                font-size:15px;
                line-height:20px;
                color:var(--input-label-placeholder);
            }
            label{
                position:absolute;
                top:0;
                font-size:12px;
                color:var(--input-label-placeholder);
                
            }
            label.error{
                color:var(--input-error);
            }

            .text-input-div{
                width:320px;
                height:88px;
                position:relative;
                margin-left:10px;
                margin-top:5px;
            }
           

        `;
    }
    constructor() {
        super();
        this.variant = VARIANT.DEFAULT;
        this.placeHolder = 'Placeholder';
        this.value = '';
        this.name = '';
        this.disabled = false;
        this.labelName = '';
        this.addEventListener('focus', this._handleFocus);
    }

    @idfyProperty({ type: String }) value;
    @idfyProperty({ type: String }) placeHolder;
    @idfyProperty({ type: Boolean }) disabled;
    @idfyProperty({ type: String }) name;
    @idfyProperty({ type: String }) labelName;
    // @idfyProperty({ type: Function }) callBackFunction: any;
    @idfyProperty({ type: String }) error = "";
    @idfyProperty({ type: VARIANT })
    variant: VARIANT;

    _handleChange() {
        const inputEvent = new CustomEvent('input-event', {
            detail: this.value,
            bubbles: true,
            composed: true
        });
        this.dispatchEvent(inputEvent);
    }

    _handleFocus() {
        console.log('foused');
    }

    update(changedProperties: any) {
        super.update(changedProperties)

        if (this.disabled) {
            this.style.setProperty("--input-border", "#E4E9F2");
            this.style.setProperty("--input-label-placeholder", "#E4E9F2");

        }

        else if (this.variant == "default") {

            // this.style.setProperty("--input-border","#FF3D71");
            // this.style.setProperty("--lit-button-font-color",this.secondary );

        }
        else if (this.variant == "success") {
            this.style.setProperty("--input-border", "#00D68F");
            this.style.setProperty("--caption-color", "#00D68F");
            // this.style.setProperty("--lit-button-font-color",this.primary);


        }
        else if (this.variant == "error") {
            this.style.setProperty("--input-border", "#FF3D71");
            this.style.setProperty("--caption-color", "#FF3D71");
            // this.style.setProperty("--lit-button-font-color",this.primary);


        }
        else if (this.variant == "warning") {
            this.style.setProperty("--input-border", "#FFAA00");
            this.style.setProperty("--caption-color", "#FFAA00");
            // this.style.setProperty("--lit-button-font-color",this.primary);


        }
        else {
            //info
            this.style.setProperty("--input-border", "#0095FF");
            this.style.setProperty("--caption-color", "#0095FF");

        }
    }

    getRender() {
        console.log(this.placeHolder);

        return tag`
        <input 
            class="input ${this.error ? "error" : ""}"
                type="text" 
                placeHolder="${this.placeHolder}" 
                value="${this.value}"
                name="${this.name}" 
                ?disabled="${this.disabled}"
                @input=${(e: any) => {
                this.value = e.target.value;
            }}
                @change=${this._handleChange}
        ></input>       
       
        `;
    }


}

customElements.define('input-lit-element', InputLitElement);

// <div class="text-input-div">  
// <label class="${this.error ? "error" : ""}">${this.error ? this.error : this.labelName}: </label>   
// <div>