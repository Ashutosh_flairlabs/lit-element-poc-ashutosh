export enum VARIANT { 
    SUCCESS = "success",
    WARNING = "warning",
    INFO = "info",
    ERROR = "danger",
    DEFAULT = "default",
    BASIC="basic"
}