import IDfyComponent, { idfyProperty, tag, idfyCss, idfyPropertyValue, idfyQueryAll } from '../IDfyComponent';

class MultiDropdownElem extends IDfyComponent {

  static override get styles() {
    return idfyCss`
    :root{
      --idfy-dropdown-background-color:"#fff";
      --idfy-dropdown-border-color:"#1C43B9";
      --idfy-dropdown-font-color: "#8F9BB3";
      --idfy-dropdown-disable-font-color:"#8F9BB3";
      }
      .multipleSelection {
        width: 320px;
        left: 1764px;
        top: -248px;
        border-radius: 0px;
        border:none;
        background-color:white;   
        }
    .selectBox {
        position: relative;
        transition: all 2s ease-in;   
    } 
    .selectBox select {
        width: 100%;
        font-weight: bold;
        padding: 12px 16px;  
    }
    .overSelect {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0; 
    }
    #checkBoxes {
      margin-top:4px;
      padding:12px,14px; 
      width:316px;
      border: 2px solid #8F9BB3;
      border-radius:5px;
      position: absolute;
      background-color: #FFFFFF;
      min-width: 160px;
      box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }
    .hide{
        display:none;
        }
    .show{
        display:block
    }
    #checkBoxes label {
          display: felx;
          align-items:center;
    }
    .selected{
      padding: 12px 16px;
      border: 1px solid #000;
      border-radius:4px;
      position:relative; 
    }
    .selected.add{
      background-color:#red;
      border:2px solid #1C43B9;
    }
    input[type="checkbox"]{
      width:20px;
      height:20px;
      background-color:#1437A3;
      margin-right:10px;
    }
    .option{
      text-align:center;
      display:block;
      transition: border 0.1s ease;
      background:white;
      padding :4px;  
    }
    .option:hover{
      border:2px solid, var(--idfy-dropdown-border-color,black);
      background-color:var(--idfy-dropdown-background-color,#EDF1F7);
    }
    .option:focus {
      color:red; 
  }
    .ioption{
        font-family: roboto;
        font-style: bold;
        width:100%;
        text-align: justify;
        display:flex;
        padding: 8px 13px;
        font-size:15px; 
        color:var(--idfy-dropdown-disable-font-color,black);
    }
    .ioption:hover{
      color:var(--idfy-dropdown-font-color);
    }
    .middlebox{
      right:0;
      position:absolute;
      display:flex;
      top:16.5px;
      bottom:17.5px;
      right:19px;
     }
      .down-icon{
        align-items:center;
        justify-content:center;
        align-self:stretch;
        width:20%;
        margin-bottom:40%;
        border-left:1px solid var(--down-icon-border);        
        cursor: pointer;
      }
      /* arrow css */
      #arrow{
        transition: all 0.3s ease ;
      }
      #arrow.up{
        transform:rotate(180deg);
      }
      /* change arrow(chevron) style on click */
      #arrow.up.add{
        transform:rotate(360deg);
      }
      #arrow.add{
        transform:rotate(180deg);
      }
    `;
  }

  update(changedProperties: any) {
    super.update(changedProperties)
    console.log(this.status);
    if (this.status == "active") {
      if (this.mutiDropdown === "false") {
        this.style.setProperty("--idfy-dropdown-background-color", "var(--brand-primary-500)");
        this.style.setProperty("--idfy-dropdown-border-color", "var(--brand-primary-500)");
        this.style.setProperty("--idfy-dropdown-font-color", 'var(--brand-basic-100)');
      }
      else {
        this.style.setProperty("--idfy-dropdown-background-color", 'var(--brand-basic-300)');
        this.style.setProperty("--idfy-dropdown-border-color", "var(--brand-primary-500)");
      }
    }
    else if (this.status == "disabled") {
      this.style.setProperty("--idfy-dropdown-background-color", 'var(--brand-basic-100)');
      this.style.setProperty("--idfy-dropdown-border-color", 'var(--brand-basic-100)');
      this.style.setProperty("--idfy-dropdown-disable-font-color", 'var(--brand-basic-400)');
      this.style.setProperty("--idfy-dropdown-font-color", 'var(--brand-basic-400)');
    }
    else {
      this.style.setProperty("--idfy-dropdown-background-color", 'var(--brand-basic-300)');
      this.style.setProperty("--idfy-dropdown-border-color", "var(--brand-primary-500)");
    }

  }
  @idfyProperty({ type: String }) status;
  @idfyProperty({ type: Array }) options: any;
  @idfyProperty({ type: Object }) myProp: any;
  @idfyProperty({ type: Number }) select;
  @idfyProperty({
    type: Boolean,
    reflect: true,
  }) disabled;
  @idfyProperty({ type: Function }) onClick: any;
  show: boolean = true;
  optionsArr: string[] = [];
  result: string[] = [];
  @idfyProperty({ type: String }) mutiDropdown;

  @idfyQueryAll('.messageCheckbox')
  _messageCheckbox!: NodeListOf<HTMLInputElement>
  constructor() {
    super();
    this.select = "Select";
    this.status = '';
    this.disabled = false;
    this.mutiDropdown = "";
    // this.addEventListener('click', this._handleClick);
  }

  override shouldUpdate(changedProperties: idfyPropertyValue) {
    return changedProperties.has('select');
  }

  _handleClick() {
    console.log('hello')
    var arrow = this.renderRoot.querySelector("#arrow")
    var checkboxes = this.renderRoot.querySelector('#checkBoxes');
    var selectBox = this.renderRoot.querySelector('.selected');
    selectBox?.classList.toggle("add");
    if (this.show) {
      checkboxes?.classList.remove("hide");
      checkboxes?.classList.add("show");
      arrow?.classList.remove("down");
      arrow?.classList.add("up");
      this.show = false;

    }
    else {
      // checkboxes?.setAttribute("display", "none");
      checkboxes?.classList.remove("show");
      checkboxes?.classList.add("hide");
      arrow?.classList.remove("up");
      arrow?.classList.add("down");
      this.show = true;
    }
  }

  _handleOption(value: any) {
    if (this.optionsArr.indexOf(value) > -1) {
      this.optionsArr.splice(this.optionsArr.indexOf(value), 1);
    } else {
      this.optionsArr.push(value);
    }
    if (this.optionsArr.length === 0) {
      // this.setAttribute('select', ``); 
      this.select = `Select`;
    } else {
      this.setAttribute('select', `${this.optionsArr.length} / ${this.options.length}`);
      this.select = `${this.optionsArr.length} / ${this.options.length}`;
    }
    let dropDownEvent = new CustomEvent('dropdown-event', { detail: this.optionsArr });
    this.dispatchEvent(dropDownEvent);
  }

  handleDropdown(value: any) {
    this.select = value;
    let dropDownEvent = new CustomEvent('dropdown-event', { detail: value });
    this.dispatchEvent(dropDownEvent);
  }
  getRender() {
    return tag`
    <div class="multipleSelection">
    <div class="selectBox" @click=${() => { this._handleClick() }}  >
        <div class="selected">
            <option class="select">${this.select}
            </option>
            <div class="middlebox">
            <svg id="arrow" class="down" width="10" height="7" viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
             <path fill-rule="evenodd" clip-rule="evenodd" d="M5.00001 6.50001C4.74401 6.50001 4.48801 6.40201 4.29301 6.20701L0.293006
              2.20701C-0.0979941 1.81601 -0.0979941 1.18401 0.293006 0.793006C0.684006 0.402006 1.31601 0.402006 1.70701 0.793006L5.01201 4.09801L8.30501 0.918006C8.70401 0.535006 9.33501 
              0.546006 9.71901 0.943006C10.103 1.34001 10.092 1.97401 9.69501
               2.35701L5.69501 6.21901C5.50001 6.40701 5.25001 6.50001 5.00001 6.50001Z" fill="black"/>
             </svg>
        </div>
        </div>
    </div>
  <div id="checkBoxes" style={background:red} class="hide">
  ${this.options.map((i: any) => tag`
  <div class="option">
  <label class="ioption"  @click=${() => { this.mutiDropdown === "false" ? (this.handleDropdown(i), this._handleClick()) : null }} for="${i}" value=${i}>
 ${this.mutiDropdown === "true" ? tag`<input @click=${() => { this._handleOption(i) }} type="checkbox" id = "${i}"/>` : null} 
   ${i}
  </label>
  </div>
  `)}
  </div>
</div>
`
  }
}

customElements.define('multidropdown-elem', MultiDropdownElem);