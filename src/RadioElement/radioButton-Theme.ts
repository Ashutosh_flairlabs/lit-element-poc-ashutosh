import IDfyComponent, { idfyCss, idfyProperty, tag } from '../IDfyComponent';

class RadioButtonTheme extends IDfyComponent {
  static override get styles() {
    return idfyCss`
         .container {
          display: block;
          position:relative;
          padding-left:33px;
          margin-bottom:7px;
          cursor: pointer;
          line-height: 24px;
          margin-left:5px;
          margin-top:10px;
        }
      /* Hide the default radio button */
      .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
      }
      /* custom radio button */
      .check {
        position: absolute;
        top: 0;
        left: 0;
        height:20px;
        width: 20px;
        background-color: var(--color-200,rgba(143, 155, 179, 0.16));
        border-radius: 50%;
        border:1px solid var( --color-500 ,#C5CEE0);
     } 
      .container:hover input ~ .check {
        border:1px solid var( --color-500 ,#C5CEE0);
          box-shadow: 0px 0px 5px 2px #8F9BB3;
     }
      .container input:checked ~ .check {
        background-color:#fff;
        box-shadow: 0px 0px 4px 1px #8F9BB3;
        border:1px solid var(--color-500,#1C43B9);

      }
      .check:after {
        content: "";
        position: absolute;
        display: none;
      }
      .container input:checked ~ .check:after {
        display: block;
      } 
      .container .check:after {
        position: absolute;
        width: 12px;
        height: 12px;
        left: calc(50% - 12px/2);
        top: calc(50% - 12px/2);
        border-radius:50%;
        background: var(--color-500, #1C43B9);
      }

      .primary {
        --color-500: var(--brand-primary-500); 
        --color-200: var(--brand-primary-100);
      }
      .success {
        --color-500: var(--brand-success-500); 
        --color-200: var(--brand-success-200); 
      }
      .warning {
        --color-500: var(--brand-warning-500);
        --color-200: var(--brand-warning-200);  
      }
      .danger {
        --color-500: var(--brand-danger-500);
        --color-200: var(--brand-danger-200);  
      }
      .info {
        --color-500: var(--brand-info-500); 
        --color-200: var(--brand-info-200);  
      }
      .disabled{
        --color-500:var(--brand-basic-500);
        --color-200:var(--brand-basic-200); 
      }
    `
  }

  constructor() {
    super();
    this.data = [];
    this.onClick = () => { }
  }
  @idfyProperty({ type: Object }) data = [];
  @idfyProperty({ type: Function }) onClick: any;
  @idfyProperty({ type: String }) type = "";
  @idfyProperty({ type: String }) fontColor = "";

  private toggleCheck(e: Event) {
    const value = (e.target as Element).getAttribute('value')!;
    let radioEvent = new CustomEvent('radio-event', {
      detail: value,
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(radioEvent);
  }

  getRender() {
    return tag`
    ${this.data.map((item: any) => tag`
    <label class="container ${this.type}">
    <input type="radio" ?checked=${item.checked} ?disabled=${item.disable} name="radio" value=${item.text} @click=${this.toggleCheck}>
    <span class="check ${this.type}"></span>
    ${item.text}
  </label>`)}
  `;
  }
}
customElements.define('radio-theme', RadioButtonTheme);