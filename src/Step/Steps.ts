import IDfyComponent, {idfyProperty, tag , idfyCss} from '../IDfyComponent';
class Step extends IDfyComponent {
    values: any;
    // disable:Boolean;

    constructor() {
        super()
        this.completed = false
        this.active = false
        // this.disable = false;

    }

    static override get styles() {
        return idfyCss`
            :host {
                --active-border-color:#1C43B9;
                --active-color:#1C43B9;
                --active-bg-color:#1C43B914;
                --inactive-color:#8F9BB3;
                --step-font-size:12px;
            }
            .step{
                display: flex;
                flex-direction: column;
                align-items: center;
                font-family: Roboto;
                cursor:pointer;
                
            }
            .step.disabled{
               
                pointer-events: none;
            }

            /*step-count*/
            .step-count{
                display: flex;
                align-items: center;
                justify-content: center;
                height: 32px;
                width: 32px;
                font-size: var(--step-font-size);
                font-weight: 600;
                border-radius: 50%;
            }
            .step-count.active {
                border: 1px solid var(--active-color);
                background: var(--active-bg-color);
                color: var(--active-color);
            }

            /*Experimental*/ 
            .step-count.done.active {
                border: 1px solid var(--active-color);
                background: var(--active-bg-color);
                color: var(--active-color);
            }

            .step-count.inactive {
                border: 1px solid var(--inactive-color);
                color: var(--inactive-color);
            }
            .step-count.done {
                border: 1px solid var(--active-color);
                color: #fff;
                background: var(--active-color);
            }
            .step-count.done.disabled{
                cursor:not-allowed;
                pointer-events: none;
            }

            /*step-state*/
            .step-state.done.disabled {
                cursor:not-allowed;
                pointer-events: none;
            }
            .step-state.done {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }
            .step-state.active {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            } 
            /*Experimental*/ 
            .step-state.done.active {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }  
            .step-state.done.inactive {
                color: var(--active-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }


            .step-state.inactive {
                color: var(--inactive-color);
                font-size: var(--step-font-size);
                font-weight: 700;
            }

        `
    }

    @idfyProperty({type:Boolean})
    disabled = false

    @idfyProperty({type:Boolean})
    completed = false;

    @idfyProperty({type:Boolean})      
    active =true; 

    @idfyProperty({type:String})
    state = "completed"

    @idfyProperty({type:String})
    count = "0"

    @idfyProperty({type:Array})
    stepsData = []

    @idfyProperty({type:Function})
    onClick = ()=>{
       
    }

    @idfyProperty({type:String})   
    icon = "../public/done_icon.svg"

    @idfyProperty({type:Number})
    index = 0;

  

    @idfyProperty({type:Function})
    clickOnStep = ()=>{
        //console.log(this.stepsData);
    
        const stepCount = this.renderRoot.querySelector(".step-count");
        const stepState = this.renderRoot.querySelector(".step-state");
        if(stepCount?.classList.contains("done") && stepState?.classList.contains("done")){
            this.active=true;
            //
            //this.completed=false;
        }
        const onChangeEvent = new CustomEvent('updateCurrentStep', {
            detail: { "value": this.count  },
            bubbles: true,
            composed: true
          });
          this.dispatchEvent(onChangeEvent);
        this.requestUpdate();
        // this.active=true;
    }

    
    getRender(){    
       
        
          
        return tag `
            <div class="step ${this.disabled ? "disabled" : "" }" part="step" id = "${this.index}">
                <div class="step-count ${this.completed ? 'done' : "" } ${this.active ? 'active' : 'inactive'} ${this.disabled ? "disabled" : ""}" @click=${this.clickOnStep}>
                 ${this.active ? this.count : this.completed ? tag`<img class="step-icon" src=${this.icon}>` : this.count}
                </div> 
                <div class="step-state ${this.completed ? 'done' :""} ${this.active ? 'active' : 'inactive'} ${this.disabled ? "disabled" : ""}">
                    ${this.active ? 'Active' : this.completed ? 'done' : 'Inactive'}
                </div>
            </div>
        `;
    }
}

customElements.define('step-element', Step);
