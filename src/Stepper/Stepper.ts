
// import {html} from 'lit-element';
import IDfyComponent, {idfyProperty, tag , idfyCss } from '../IDfyComponent';

class Stepper extends IDfyComponent {
  
 
    // @idfyProperty({type:String})
    // idName = "#1"

    // @idfyQuery("step") _inputElement: any;

    @idfyProperty({type:Function})
    onNext = ()=>{
        
        if(this.currentStep > this.steps.length - 1 ){
            this.disabled = true;
        }
        
        if(this.currentStep == this.steps.length - 1){
          
            this.steps[this.currentStep ].isDone = true
            this.steps[this.currentStep ].isActive = false
           
            this.nextLabel =  'SUBMIT';
            // let allSteps = this.renderRoot.querySelectorAll(".step-count");
            // console.log(allSteps);
            this.currentStep+=1;
            
        }
        if(this.currentStep < this.steps.length - 1){
     
     
            this.steps[this.currentStep].isActive = false
            this.steps[this.currentStep].isDone = true
            this.currentStep+=1
            this.steps[this.currentStep ].isActive = true 

          
        }
        this.requestUpdate()
    };
    @idfyProperty({type:Function})
    onPrev = ()=>{
        if(this.currentStep == 0){
            this.steps[this.currentStep ].isDone = false;
            this.steps[this.currentStep].isActive = true;
            this.nextLabel =  'NEXT';
        }
       
        if(this.currentStep > 0 && this.currentStep <= this.steps.length - 1){
            this.steps[this.currentStep ].isDone = false;
            if(this.steps[this.currentStep ].isActive == false){
                this.steps[this.currentStep ].isActive = true;
                this.nextLabel = "NEXT";
            }
            else if(this.steps[this.currentStep ].isActive == true){
                this.steps[this.currentStep ].isActive = false;
                this.currentStep-=1
                this.steps[this.currentStep ].isActive = true;
                this.steps[this.currentStep ].isDone = false;
                this.nextLabel = "NEXT";
            }
            
        }
        this.requestUpdate()
    };
    @idfyProperty({type:Function})
    onClick = (e:any)=>{
       
        this.currentStep = e.target.count - 1;
       
        // this.disableBtn = false;
        this.steps.map((step)=>{
            // console.log("inside map");
            if(step.isActive === true){
                step.isActive = false;
                // step.isDone = true;
            }
            if(step.isDone){
                step.isDone = true;
            }
            
        })
    //    console.log(this.steps);
       
        this.steps[e.target.count - 1].isActive = true;
        // console.log(this.renderRoot.querySelector("."));
        
        // this.steps[e.target.count - 1].isDone = true;
     
        this.requestUpdate()
    }
    @idfyProperty({type:Function})
    updateState = ()=>{
        this.requestUpdate()
    };
    @idfyProperty({type: Function})
    _setSteps = () => {
        console.log('set Steps event called')
    }
    @idfyProperty({type:Array})
    steps=[
        {
            stepNo: 1,
            isActive: true,
            isDone: false,
            finalState:false

        },
        {
            stepNo: 2,
            isActive: false,
            isDone: false,
            finalState:false
        },
        {
            stepNo: 3,
            isActive: false,
            isDone: false,
            finalState:false
        }
    ];
    @idfyProperty({type:Boolean})
    isActive = true;
    @idfyProperty({type:Boolean})
    disabled = false;
    @idfyProperty({type:Boolean})
    isDone = false;
    @idfyProperty({type:String})
    nextLabel = 'NEXT';
    @idfyProperty({type:String})
    prevLabel = 'PREVIOUS';
    @idfyProperty({type:Number})
    currentStep = 0
    @idfyProperty({type:String})   
    icon = ""

    constructor() {
        super();
        const onChangeEvent = new CustomEvent('getSteps', {
            detail: { "value": this.steps  }
          });
          this.dispatchEvent(onChangeEvent);

          this.addEventListener('setSteps', this._setSteps);

        //   const setSteps = new CustomEvent('setSteps', {
        //     detail: { "value": this.steps  }
        //   });
        //   this.dispatchEvent(setSteps);
       
        // setInterval(() => {
        //     this.requestUpdate()
        // }, 1000)
            
    }

    // shouldUpdate(changedProperties : any) {
    //     changedProperties.forEach((oldValue:any, propName:any) => {
    //       console.log(`${propName} changed. oldValue: ${oldValue}`);
    //     });
    //     return changedProperties.has('prop1');
    //   }



    static override get styles() {
        return idfyCss`
            .container {
                display: flex;
                flex-direction: column;
                border: 1px solid #c5c5c5;
            }
            .step-container {
                display: flex;
                justify-content: space-around;
                padding:10px;
                margin: 10px;
            }
            .disabled{
                cursor:not-allowed !important;
                pointer-events: none;
            }
            .btn-container{
                padding:10px;
                display: flex;
                justify-content: flex-end;
            }
            .btn {
                background-color: var(--active-color);
                box-shadow: 0px 8px 15px rgdisableStepba(0, 0, 0, 0.1);
                color: var(--btn-font-color);
                padding: 5px 10px;
                margin: 5px;
                font-weight: 700;
                border: none;
                border-radius: 4px;
                outline: none;
                cursor: pointer;
            }
            .btn.disable{
                cursor:not-allowed !important;
                pointe_handleFocus
        `
        }

    getRender() {
   
//    console.log("After" , this.steps);
    
        return tag`
        <div class="container" >
            <div class="step-container">
                ${this.steps.map((e,index) => tag`<step-element class="${this.disabled?"disabled":""}" ?disabled=${this.disabled}  .values="${this.steps}" icon=${this.icon}  index=${index}  count=${e.stepNo} ?completed=${e.isDone} ?active=${e.isActive} @click = "${this.onClick}" ></step-element>`)}
            </div>
            <div class="btn-container">
              
            </div>
        </div>
        `
    }
}

customElements.define('stepper-element', Stepper);
// <button class="btn ${this.disabled ? "disable" : ""}" part="step-btn" @click=${this.onPrev}>${this.prevLabel}</button>
// <button class="btn ${this.disabled ? "disable" : ""}" part="step-btn" @click=${this.onNext}>${this.nextLabel}</button>