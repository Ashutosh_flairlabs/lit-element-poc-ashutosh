interface Provider {
    region: string,
    country: string,
    locale: string,
    company: string
}