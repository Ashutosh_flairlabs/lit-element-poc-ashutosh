import IDfyComponent, { idfyCss, idfyProperty, tag } from '../IDfyComponent';

class ToggleSwitchElement extends IDfyComponent {
  static override get styles() {
    return idfyCss`
        .switch {
            position: relative;
            display: block;
            width: 60px;
            height: 34px;
            margin-top:10px;
            margin-left:5px;
          }
          .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
            position: absolute;
          }
          .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color:var(--color-200,#ccc);
            border:1px solid var(--color-200,#ccc);
            -webkit-transition: .4s;
            transition: .4s;
          }
          
          .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 1.5px;
            bottom: 3px;
            background-color: white;
           
          }
          input:checked + .slider {
            background-color:var(--color-500,#1C43B9);/* after checked background color*/
          }
          input:hover + .slider{
            border: 1px solid var(--color-500, #fff);
            box-shadow: 0px 0px 8px 1px rgba(143, 155, 179, 0.60);
          }
          
          input:checked + .slider:before {
             -webkit-transform: translateX(28px);
             -ms-transform: translateX(28px);
             transform: translateX(28px);
          }
          
          /* Rounded sliders */
          .slider.round {
            border-radius: 34px;
          }
          .slider.round:before {
            border-radius: 50%;
          }
          
          .svg {
            top:12px;
            left:10px;
            position:absolute;
            -webkit-transition: .4s;
            transition: .4s;
            -webkit-transform: translateX(28px);
            -ms-transform: translateX(28px);
             transform: translateX(28px);
            } 

            .iconColor{
              fill:var(--color-500,#1C43B9);
            }

          .primary {
            --color-500: var(--brand-primary-500); 
            --color-200: var(--brand-primary-100);
          }
          .success {
            --color-500: var(--brand-success-500); 
            --color-200: var(--brand-success-200); 
          }
          .warning {
            --color-500: var(--brand-warning-500);
            --color-200: var(--brand-warning-200);  
          }
          .danger {
            --color-500: var(--brand-danger-500);
            --color-200: var(--brand-danger-200);  
          }
          .info {
            --color-500: var(--brand-info-500); 
            --color-200: var(--brand-info-200);  
          }
          .disabled{
            --color-500:var(--brand-basic-500);
            --color-200:var(--brand-basic-200); 
          }
    `
  }
  constructor() {
    super();
    this.disabled = false;
    this.onClick = () => { }
    this.value = false;
  }
  @idfyProperty()
  value: boolean;
  @idfyProperty({ type: Function }) onClick: any;
  @idfyProperty({ type: String }) type = "";
  @idfyProperty({ type: String }) label = "On";
  @idfyProperty({ type: String }) checked = "";
  @idfyProperty()
  disabled: boolean;

  private toggleCheck(e: any) {
    this.value = e.target.checked
    console.log(' e.target.checked', e.target.checked)
    let inputSwtich = new CustomEvent('switch-event', {
      detail: e.target.checked,
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(inputSwtich);
  }

  getRender() {
    console.log(this.checked);
    return tag`
        <label class="switch ${this.type}">

        <input type="checkbox" ?checked=${this.checked} ?disabled=${this.disabled} @click=${this.toggleCheck}>
        <span class="slider round">
        ${this.value === true || this.checked ? tag`
        <svg class='svg'  width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" class="iconColor" clip-rule="evenodd" d="M9.0578 1.1439C9.58889 0.443064 10.6077 0.290736 11.3333 0.803661C12.059 1.31659 12.2167 2.30053 11.6856 3.00136L6.30595 10.1005C5.94354 10.5787 5.2445 10.6323 4.81338 10.215L0.475519 6.01544C-0.159574 5.40061 -0.158348 4.40494 0.478256 3.79157C1.11486 3.17819 2.14577 3.17937 2.78087 3.79421L5.24308 6.17789L9.0578 1.1439Z" fill="#00D68F"/>
        </svg>
        `: null} 
        </span>

        </label>

  `;
  }

}
customElements.define('toggle-switch-element', ToggleSwitchElement);