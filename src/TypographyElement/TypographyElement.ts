import IDfyComponent, { tag, idfyProperty } from '../IDfyComponent';
import {typographyStyle} from "./typographyStyle"

class TypographyElement extends IDfyComponent {


    static override get styles() {;
      return typographyStyle 
    }
  

  @idfyProperty({type: String}) variant = "h1";
  @idfyProperty({type: String}) component= "div"

  constructor() {
    super();
  }

  update(changedProperties : Map<string | number | symbol, unknown>) {
    super.update(changedProperties)
}



  getRender() {
    console.log('ewngowE;NON',this.variant,this.component );
    return tag`
    <h1 class="${this.variant}" >
    <slot></slot>
    </h1>`
    ;
  }
}

customElements.define('typography-element', TypographyElement);

